#!/bin/bash

# Supprime les retours chariot windows dans les fichiers de sources

sudo sed -i 's/\r//' /home/git/droits/backoffice/dirs_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/backoffice/files_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/backoffice/users_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/bo/dirs_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/bo/files_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/bo/users_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/php_commun/dirs_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/php_commun/files_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/php_commun/users_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/tools/dirs_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/tools/files_authorized.txt
sudo sed -i 's/\r//' /home/git/droits/tools/users_authorized.txt