#!/usr/bin/env bash

# MAJ Rancher à tester sur play-with-docker
# testUpdateRancherPlayWithDocker.sh
# Bash3 Boilerplate. Copyright (c) 2014, kvz.io
# http://kvz.io/blog/2013/11/21/bash-best-practices/

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

ID_PLAY_WITH_DOCKER=ip172-18-0-12-bc5uehf9lphg009tj3tg
TOKEN_PLAY_WITH_DOCKER=2DC44D4B39E59098E2AF:1514678400000:DXPQVo3jJYFNXuxHnPp2CSZeks

RANCHER_OLD_VERSION=rancher/server:v1.6.7
RANCHER_NEW_VERSION=rancher/server:v2.0.0-beta4

DNS_PLAY_WITH_DOCKER=direct.labs.play-with-docker.com
PLAY_WITH_DOCKER=${ID_PLAY_WITH_DOCKER}@${DNS_PLAY_WITH_DOCKER}

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app
arg1="${1:-}"


# si le parametre 1 est --help alors affiche l'aide
if [ "$arg1" = "--help" ] || [ "$arg1" = "-h" ] || [ "$#" -gt 1 ]; then
    if [ "$#" -gt 1 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
testUpdateRancherPlayWithDocker.sh
==================================

Ce script permet de déployer rapidement un rancher "similaire" à notre configuration sicav, afin de tester sa mise à jour et de sécuriser celle-ci pour notre production.

Exemple d'utilisation :

$ ./testUpdateRancherPlayWithDocker.sh
EOF
  exit 0
fi

SHA1_DOCKER_RANCHER_SERVER=$(ssh sicav-preprod1 docker ps --filter='name=rancher-server' -q)

echo "'df -h' sur ${PLAY_WITH_DOCKER}"
ssh ${PLAY_WITH_DOCKER} df -h

# echo "'docker version' sur ${PLAY_WITH_DOCKER}"
# ssh ${PLAY_WITH_DOCKER} docker version

# echo ""
# echo "'docker pull ${RANCHER_OLD_VERSION}' sur ${PLAY_WITH_DOCKER}"
# ssh ${PLAY_WITH_DOCKER} docker pull ${RANCHER_OLD_VERSION}

echo ""
echo "'docker run ... ${RANCHER_OLD_VERSION}' sur ${PLAY_WITH_DOCKER}"
ssh ${PLAY_WITH_DOCKER} docker run -d --restart=unless-stopped -p 8080:8080 ${RANCHER_OLD_VERSION}


# Commandes executables sur https://labs.play-with-docker.com/
#
# ID_PLAY_WITH_DOCKER=ip172-18-0-12-bc5uehf9lphg009tj3tg; \
# TOKEN_PLAY_WITH_DOCKER=2DC44D4B39E59098E2AF:1514678400000:DXPQVo3jJYFNXuxHnPp2CSZeks; \
# DNS_PLAY_WITH_DOCKER=direct.labs.play-with-docker.com; \
# docker run --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.5 http://${ID_PLAY_WITH_DOCKER}-8080.${DNS_PLAY_WITH_DOCKER}/v1/scripts/${TOKEN_PLAY_WITH_DOCKER}





exit 0
