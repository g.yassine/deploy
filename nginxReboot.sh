#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
#set -o xtrac

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app

arg1="${1:-}"

LOG_FILE="/var/log/letsencrypt/certbot-renew.log"
DATE="$(date)"
NAME_CONTAINER_REVERSE="reverse"

echo "Check et reboot du serveur Ngnix ${DATE}" | sudo tee -a ${LOG_FILE} \
&& docker exec -ti ${NAME_CONTAINER_REVERSE} nginx -t | sudo tee -a ${LOG_FILE} \
&& docker exec -ti ${NAME_CONTAINER_REVERSE} nginx -s reload | sudo tee -a ${LOG_FILE}