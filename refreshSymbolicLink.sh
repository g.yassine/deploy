#!/bin/bash

project=$1

if [ "$1" = "--help" ] || [ "$1" = "-h" ] || [ "$#" -ne 1 ]; then
    if [ "$#" -ne 1 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
    refreshSymbolicLink.sh
    ======================

    Ce script permet de mettre à jour le lien symbolique du projet en question (backoffice / BO / Tools...)

    Exemple d'utilisation :

    $ ./refreshSymbolicLink.sh [project]    
EOF
    exit 0
fi

# Variables
dirHttp="/home/httpd"
dirDeploy="/home/app/deploy"
lastRelease=$(ls -rt $dirDeploy/$project/releases | tail -n1)
targetLastRelease="$dirDeploy/$project/releases/$lastRelease"
link=$dirHttp/$project
currentTarget=$(ls -lh $link | awk '{print $11}')

# MAIN
if [ $(sudo whoami) != "root" ]; then
  echo "Votre utilisateur '$(whoami)' n'a pas les droits sudo."
  exit 1
fi

echo "Le lien actuel est : $currentTarget"
if [ "$targetLastRelease" != "$currentTarget" ]; then
    sudo rm $link
    ln -s $targetLastRelease $link
    sudo chown git:info $link
    echo "Le nouveau lien designe : $targetLastRelease"
else
    echo "La version actuelle est correcte."
fi

exit 1
