#!/usr/bin/env bash
# Bash3 Boilerplate. Copyright (c) 2014, kvz.io
# http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace
# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app
arg1="${1:-}"

. ${__dir}/../functions.sh

projet="test"
current=$(date +%Y%m%d%H%M%S)
nouveauTag="1.x.x - ${current}"
dirSave=${__dir}/tmp
destinatairesMail="s.salles@sicavonline.fr" # adresses séparées par des espaces
dirLogs=/tmp

notifmail
