#!/bin/env bash

#ToDo : S'assurer qu'on a les droits pour le sudo
#       S'assurer qu'on est est bon endroit

DIR="../"


echo -e "\n\033[32mcore, CoreBundle/Resources/config/parameters.yml :\033[0m"
cat ${DIR}core/vendor/sicav/CoreBundle/Resources/config/parameters.yml|grep databa

echo -e "\n\033[33mcore, app/config/parameters.yml :\033[0m"
cat ${DIR}core/app/config/parameters.yml|grep odbc_
echo -e "\n\033[31mSuppression du cache dans core/app/cache/\033[0m"
sudo rm -rf core/app/cache/*

echo -e "\n\033[34mbackoffice, CoreBundle/Resources/config/parameters.yml :\033[0m"
cat ${DIR}backoffice/v2/vendor/sicav/CoreBundle/Resources/config/parameters.yml|grep databa
echo -e "\n\033[31mSuppression du cache dans backoffice/v2/var/cache/\033[0m"
sudo rm -rf backoffice/v2/var/cache/*

echo -e "\n\033[35mbackoffice, config/parameters.php :\033[0m"
cat ${DIR}backoffice/config/parameters.php|grep DB_

echo -e "\n\033[36mphp_commun, config/parameters.php :\033[0m"
cat ${DIR}php_commun/config/parameters.php|grep -E 'DEV_|NEWBACK_'

echo -e "\n\033[31mcheck version CFIDE\033[0m"
