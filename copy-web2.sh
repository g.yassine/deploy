#!/bin/env bash

# dossier à importer depuis le serveur distant
SERVER="sicav-web2"
DIR_LETS="/usr/local/httpd/conf/extra/letsencrypt"

DIR_DEST="/data/nginx/letsencrypt/live"

# NE RIEN MODIFIER APRES CETTE LIGNE

FILE_TAR_DIST="/tmp/lets.tar.gz"
FILE_TAR_LOCAL="/tmp/ici.tar.gz"
DIR_TMP_LOCAL="/tmp"
DIR_UNTAR_LOCAL="/tmp/usr"

ssh -t ${SERVER} "sudo tar cfz ${FILE_TAR_DIST} ${DIR_LETS}"

scp ${SERVER}:${FILE_TAR_DIST} ${FILE_TAR_LOCAL}

tar xfz ${FILE_TAR_LOCAL} -C ${DIR_TMP_LOCAL}

tree -C ${DIR_UNTAR_LOCAL}

cp "${DIR_UNTAR_LOCAL}/pra-af" "${DIR_DEST}/pra-af"
cp "${DIR_UNTAR_LOCAL}/pra-afim" "${DIR_DEST}/pra-afim"
cp "${DIR_UNTAR_LOCAL}/pra-afin" "${DIR_DEST}/pra-afin"
cp "${DIR_UNTAR_LOCAL}/pra-aia" "${DIR_DEST}/pra-aia"
cp "${DIR_UNTAR_LOCAL}/pra-nc2" "${DIR_DEST}/pra-nc2"
cp "${DIR_UNTAR_LOCAL}/recette-ariane" "${DIR_DEST}/recette-ariane"
cp "${DIR_UNTAR_LOCAL}/www.ageas-patrimoine.fr" "${DIR_DEST}/www.ageas-patrimoine.fr"

cp "${DIR_UNTAR_LOCAL}/backoffice.sicavonline.fr" "${DIR_DEST}/pra-back"
cp "${DIR_UNTAR_LOCAL}/bo.sicavonline.fr" "${DIR_DEST}/pra-bo"
cp "${DIR_UNTAR_LOCAL}/scpi.sicavonline.fr" "${DIR_DEST}/pra-scpi"
cp "${DIR_UNTAR_LOCAL}/studio.sicavonline.fr" "${DIR_DEST}/pra-studio"
cp "${DIR_UNTAR_LOCAL}/www.sicavonline.fr" "${DIR_DEST}/pra-www"



# adminsol@sicav-praweb ~ % ls /tmp/usr/local/httpd/conf/extra/letsencrypt/backoffice.sicavonline.fr -hl
# total 28K
# -rw-rwxr--. 1 adminsol adminsol 1.8K Oct  1 00:40 backoffice.sicavonline.fr.cer
# -rw-rwxr--. 1 adminsol adminsol  540 Oct  1 00:40 backoffice.sicavonline.fr.conf
# -rw-rwxr--. 1 adminsol adminsol  952 Oct  1 00:40 backoffice.sicavonline.fr.csr
# -rw-rwxr--. 1 adminsol adminsol  175 Oct  1 00:40 backoffice.sicavonline.fr.csr.conf
# -rw-rwxr--. 1 adminsol adminsol 1.7K Aug  2 11:52 backoffice.sicavonline.fr.key
# -rw-rwxr--. 1 adminsol adminsol 1.7K Oct  1 00:40 ca.cer
# -rw-rwxr--. 1 adminsol adminsol 3.4K Oct  1 00:40 fullchain.cer


# adminsol@sicav-praweb ~ % ls /tmp/usr/local/httpd/conf/extra/letsencrypt
# backoffice.advenis.com
# backoffice.avenirfinance.fr # plus utilisé : il est en redirection directe et permanente ver backoffice.advenis.com
# fip-fcpi.sicavonline.fr

# [root@sicav-web2 /]# acme.sh --list
# Main_Domain                  KeyLength  SAN_Domains  Created                         Renew
# backoffice.advenis.com       ""         no           Sat Sep 30 22:40:12 UTC 2017    Wed Nov 29 22:40:12 UTC 2017
# backoffice.avenirfinance.fr  ""         no           Sun Oct  1 22:40:09 UTC 2017    Thu Nov 30 22:40:09 UTC 2017
# backoffice.sicavonline.fr    ""         no           Sat Sep 30 22:40:18 UTC 2017    Wed Nov 29 22:40:18 UTC 2017
# bo.sicavonline.fr            ""         no           jeu. août 17 14:49:51 UTC 2017  lun. oct. 16 14:49:51 UTC 2017
# fip-fcpi.sicavonline.fr      ""         no           Sat Sep 30 22:40:24 UTC 2017    Wed Nov 29 22:40:24 UTC 2017
# scpi.sicavonline.fr          ""         no           Mon Oct  9 22:40:08 UTC 2017    Fri Dec  8 22:40:08 UTC 2017
# studio.sicavonline.fr        ""         no           Sat Sep 30 22:40:29 UTC 2017    Wed Nov 29 22:40:29 UTC 2017
# www.sicavonline.fr           ""         no           Thu Sep 28 22:40:07 UTC 2017    Mon Nov 27 22:40:07 UTC 2017
# You have new mail in /var/spool/mail/root


# adminsol@sicav-praweb ~ % sudo tree -C /data/nginx/letsencrypt/live
# /data/nginx/letsencrypt/live
# ├── pra-af
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── pra-afim
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── pra-afin
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── pra-aia
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── pra-back
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── pra-bo
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── pra-nc2
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── pra-scpi
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── pra-studio
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── pra-www
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# ├── recette-ariane
# │   ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
# │   ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
# │   ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
# │   └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem
# └── www.ageas-patrimoine.fr
#     ├── cert.pem -> ../../archive/www.ageas-patrimoine.fr/cert5.pem
#     ├── chain.pem -> ../../archive/www.ageas-patrimoine.fr/chain5.pem
#     ├── fullchain.pem -> ../../archive/www.ageas-patrimoine.fr/fullchain5.pem
#     └── privkey.pem -> ../../archive/www.ageas-patrimoine.fr/privkey5.pem