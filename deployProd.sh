#!/usr/bin/env bash
# Bash3 Boilerplate. Copyright (c) 2014, kvz.io
# http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace
# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app
arg1="${1:-}"

# ------------------------------------------------
# Initialisation de variables
# ------------------------------------------------
dirProd="/home/httpd/prod"
dirReleaseIni="/home/httpd/sav/releases"
dirSave="/home/httpd/sav"

# Necessaire pour corriger les droits sur le serveur de test
serveurDev=false

nbBackup=4

gitServer="gitlab-sicav" # doit être défini dans /etc/hosts
gitUser="git"
gitBranch="master"
gitConfig="--depth 1 --single-branch"

groupFile="nobody"
destinatairesMail="s.salles@sicavonline.fr x.hermin@sicavonline.fr g.moulin@sicavonline.fr k.benaissa@sicavonline.fr" # adresses séparées par des espaces
dirLogs="/home/app/logs"
# ------------------------------------------------
# Fonctions
# ------------------------------------------------
. ./functions.sh

backupFiles=(
    "ged"
    "ged_ageas"
    "download"
    "img/demarcheurs"
    "img/trombino"
    "img/webcam"
    "img/biblio"
    "pages/expo"
    "pages/presse/articles"
    "pages/contacts/"
    "pages/"
    "pages/Patritheque"
)

if [ $(sudo whoami) != "root" ]; then
    echo "Votre utilisateur '$(whoami)' n'a pas les droits sudo."
    exit 1
fi

# si le parametre 1 est --help alors affiche l'aide
if [ "$1" = "--help" ] || [ "$1" = "-h" ] || [ $# -eq 0 ]; then
    if [ $# -eq 0 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
    deployProd.sh - (2017 July 10)
    ==============================

    Si un projet est déjà déployé et qu'une version GIT est celle utilisée dans le dossier $dirProd
    Alors on recupere cette version pour s'en servir comme base :
    - on copie ce dossier à l'exception des fichiers detectés dans le build.xml du projet, ils sont sensés être des 'données' à conserver.
    - on continu la mise a jour via un git fetch + rebase ...

    Attention l'utilisateur doit avoir les droits sudo

    Parametres :
    nomDuProjetGit : projet à deployer
    rollback : 'rollback' *optionel*

    Exemple d'utilisation :

    $ ./deployProd.sh [nomDuProjetGit] [rollback]
EOF
    exit 0
fi

if [ "$1" != "" ]; then
    projet="$1"
else
    echoLog "Vous devez spécifier un nom de projet à déployer en production !" alert
    exit 1
fi
current=$(date +%Y%m%d%H%M%S)
dirSave=$dirSave/$projet-$current
dirRelease=$dirReleaseIni/$current
suppressionSauvegarde=false

# ------------------------------------------------
# Main
# ------------------------------------------------
echoLog $(date +"%H:%M:%S_%d/%m/%Y" )
# si le parametre 2 est rollback alors on execute ça sinon c'est le parametre de branche
if [ "$2" = "rollback" ]; then
    echoLog "Rollback du projet '\033[32m$projet\033[0m' :"
    rollback
    notifmail $2
    exit 0
else
    echoLog "Déploiement en production de '\033[32m$projet\033[0m' ($dirSave) :"
fi

if [ -e "$dirProd/$projet/.git" ]; then
    # Le dossier en producion est bien un dossier git on peut le déployer simplement
    recupereDerniereVersionProjet
else
    # Le dossier de prod n'est pas un git on peut le cloner
    echoLog "Le projet $projet n'est pas un projet GIT, voulez-vous effectuer un git clone ? (o/n)" false
    read wantGitClone
    if [ "$wantGitClone" == "o" ]; then
        cloneProjet
    else
        echoLog "Arret du script, car vous ne souhaitez pas cloner le nouveau projet" false
        echoLog "Le dossier $dirSave/clone est toujours existant !" false
        exit 1
    fi
fi

if [ "$projet" = "backoffice" ] || [ "$projet" = "trashSymfony" ]; then
    saveDossiersBackOffice
fi
if [ "$projet" = "php_commun" ]; then
    saveDossiersPhpCommun
fi
recuperationFichiersConfig
mepNewVersion
if [ "$projet" = "backoffice" ] || [ "$projet" = "trashSymfony" ]; then
    restoreDossiersBackoffice
    checkDossiersBackOffice # suppression de dossier de sauvegarde faite si tout est ok !
fi
if [ "$projet" = "php_commun" ]; then
    restoreDossiersPhpCommun
    checkDossiersPhpCommun # suppression de dossier de sauvegarde faite si tout est ok !
fi
siProjetSymfonyInstallation
suppressionAncienneVersion
notifmail

exit 0
