#!/usr/bin/env bash

# Bash3 Boilerplate. Copyright (c) 2014, kvz.io
# http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace
# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app
arg1="${1:-}"

CHEMIN="/home/adminsol/certificats/generate"
# CHEMIN_PROXY="/data/nginx/letsencrypt/live/registry"
# CHEMIN_REGISTRY_LB="/data/registry/certs"
CHEMIN_DEST="/etc/letsencrypt/live/www.ageas-patrimoine.fr"

SRC_CA_CERT="/home/adminsol/certificats/src/cert_Geotrust_rsa_2018_1.crt"
SRC_CERTIF="/home/adminsol/certificats/src/cert_wlc.intranet.ageas.fr.p12"

CA_PEM="cert_Geotrust_rsa_2018.pem"
CERT_SITE="cert_wlc.intranet.ageas.fr.pem"
# CERT_SITE_CER="www.ageas-patrimoine.fr.cer"

CERT_FULLCHAIN="cert_wlc.intranet.ageas.fr.fullchain.pem"
KEY_SITE="cert_wlc.intranet.ageas.fr.key"
KEY_PEM_SITE="${KEY_SITE}.pem"
CRT_SITE="cert_wlc.intranet.ageas.fr.crt"
PASSWD=$(cat passwd)
TEMPORARY_PASSWD="TemporaryPassword"

# si le parametre 1 est --help alors affiche l'aide
if [ "${ARG}" = "--help" ] || [ "${ARG}" = "-h" ] || [ "$#" -ge 1 ]; then
    if [ "$#" -ge 1 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi

    cat << EOF
    cert.sh
    =======
    Generation des fichier key et crt a partir de la cle p12.
    Si ces fichiers existent déjà ils ne seront pas générés de nouveau.
EOF
    exit 0
fi

if [ ! -d $CHEMIN ]; then
    mkdir -p $CHEMIN
fi

# Generation des fichiers OK pour prise en charge par nginx
if [ ! -f $CHEMIN/$CA_PEM ]; then
    openssl x509 \
    -outform PEM \
    -in $SRC_CA_CERT \
    -out $CHEMIN/$CA_PEM # ca-crt -> ca-pem
fi

if [ ! -f $CHEMIN/$CERT_SITE ]; then
    openssl pkcs12 \
    -in $SRC_CERTIF \
    -clcerts -nokeys \
    -out $CHEMIN/$CERT_SITE # p12 -> pem
fi

if [ ! -f $CHEMIN/$KEY_PEM_SITE ]; then
    openssl pkcs12 \
    -nodes \
    -in $SRC_CERTIF \
    -out $CHEMIN/$KEY_PEM_SITE # p12 -> key.pem
fi

if [ ! -f $CHEMIN/$CERT_FULLCHAIN ]; then
    cd $CHEMIN \
    && ls -lh $CERT_SITE $CA_PEM \
    && cat $CERT_SITE $CA_PEM > $CERT_FULLCHAIN # pem + ca-pem -> fullchain.pem
fi

if [ ! -f ${CHEMIN}/${CRT_SITE} ]; then
    openssl pkcs12 \
    -clcerts -nokeys \
    -in ${SRC_CERTIF} \
    -out ${CHEMIN}/${CRT_SITE} # p12 -> crt
fi

if [ ! -f ${CHEMIN}/${KEY_SITE} ]; then
    echo "First, extract the certificate:"
    openssl pkcs12 -clcerts -nokeys -in ${SRC_CERTIF} \
    -out ${CHEMIN}/tmp-certificate-with-passwd.crt \
    -password pass:${PASSWD}\
    -passin pass:${PASSWD} # p12 -> crt_pass

    echo "Second, the CA key:"
    openssl pkcs12 -cacerts -nokeys -in ${SRC_CERTIF} \
    -out ${CHEMIN}/tmp-ca-cert-with-passwd.ca \
    -password pass:${PASSWD}\
    -passin pass:${PASSWD} # p12 -> ca_pass

    echo "Now, the private key:"
    openssl pkcs12 -nocerts -in ${SRC_CERTIF} \
    -out ${CHEMIN}/tmp-private-with-passwd.key \
    -password pass:${PASSWD} \
    -passin pass:${PASSWD} \
    -passout pass:${TEMPORARY_PASSWD} # p12 -> key_pass

    echo "Now remove the passphrase:"
    openssl rsa -in ${CHEMIN}/tmp-private-with-passwd.key \
    -out ${CHEMIN}/${KEY_SITE} \
    -passin \
    pass:${TEMPORARY_PASSWD} # key_pass -> key
fi

cat << EOF

Parametrage nginx a deployer : [$(date)]
=============================================================

scp ${CHEMIN}/cert_Geotrust_rsa_2018.pem sicav-tools2:/tmp
scp ${CHEMIN}/{${KEY_SITE},${CRT_SITE}} sicav-tools2:/tmp
sudo cp /tmp/cert_Geotrust_rsa_2018.pem /data/nginx/letsencrypt/live/registry
sudo cp /tmp/cert_Geotrust_rsa_2018.pem /data/registry/certs
sudo cp /tmp/{${KEY_SITE},${CRT_SITE}} /data/registry/certs
sudo mv -f /data/registry/certs/${KEY_SITE} /data/registry/certs/server.key
sudo mv -f /data/registry/certs/${CRT_SITE} /data/registry/certs/server.crt

sudo cp /data/nginx/letsencrypt/live/registry/cert_Geotrust_rsa_2018.pem
sudo cp /data/registry/certs/cert_Geotrust_rsa_2018.pem

export REVERSE=$(docker ps -q --filter "name=reverse"); \
export S=$(docker ps -q --filter "name=r-registry-sslproxy"); \
export R=$(docker ps -q --filter "name=r-registry-registry"); \
export P=$(docker ps -q --filter "name=r-registry-portus")

#
# REVERSE
# /data/nginx/letsencrypt les certifs, /data/sources/Dockerfile/rancher/nginx/tools les config
#
  tree -guhDt --du /data/nginx/letsencrypt /data/sources/Dockerfile/rancher/nginx/tools
  vim /data/sources/Dockerfile/rancher/nginx/tools/registry/443.conf
  dex $(docker ps -q --filter "name=reverse") nginx -t # test si la config proxy nginx est ok

#
# SSL_PROXY
# /data/registry/certs les certifs, /data/registry/proxy les config
#
  CONT_SSL_PROXY=$(docker ps -q --filter "name=r-registry-sslproxy")
  tree -guhDt --du /data/registry/certs /data/registry/proxy
  sudo vim /data/registry/proxy/portus.conf
  dex $$CONT_SSL_PROXY nginx -t # test si la config nginx est ok
  dex $$CONT_SSL_PROXY nginx -s reload # recharge la config nginx
  dmounts $$CONT_SSL_PROXY # lit les volumes montes entre le container et l'host

# sudo vim /data/registry/proxy/portus.conf
ssl_certificate         $CHEMIN_DEST/$CERT_FULLCHAIN;
ssl_certificate_key     $CHEMIN_DEST/$KEY_PEM_SITE;
# ssl_client_certificate  $CHEMIN_DEST/$CERT_FULLCHAIN;
ssl_client_certificate  $CHEMIN_DEST/$CA_PEM;
ssl_trusted_certificate $CHEMIN_DEST/$CA_PEM; # liste des CA MAIS non distribue aux clients
ssl_session_timeout 5m;

Liste des fichiers du projet :
==============================
$(tree -Dt --du ${CHEMIN})
EOF
