def test_passwd_file(File):
    passwd = File("/etc/passwd")
    assert passwd.contains("root")
    assert passwd.user == "root"
    assert passwd.group == "root"
    assert passwd.mode == 0o644


def test_nginx_is_installed(Package):
    pkg = Package("openssh")
    assert pkg.is_installed
    assert pkg.version.startswith("6")


def test_nginx_running_and_enabled(Service):
    serv = Service("sshd")
    assert serv.is_running
    assert serv.is_enabled