import pytest
import re
import multiprocessing

partition_regex = re.compile(ur'^(?P<filesystem>[^\s]+)\s+(?P<size>[0-9\.]+)(?P<unit>[GMBk])?\s+(?P<used>[^\s]+)\s+(?P<avail>[^\s]+)\s+(?P<use>[^\s]+)\s+(?P<mounted>[^\s]+)')
mem_regex = re.compile(ur'^[^\s]+\s+(?P<total>[0-9\.]+)(?P<unit>[GMBk]).+')

@pytest.mark.base
def test_adminsol(User):
    usr = User("adminsol")
    assert usr.exists
    assert usr.gid == 1000
    assert usr.uid == 1000
    assert usr.home == "/home/adminsol"

@pytest.mark.parametrize("volume, size, unit",[
	("/var","60","G"),
	("/tmp","10","G"),
	("/data","700","G"),
	("/","50","G")
	])
def test_partition(Command, volume, size, unit):
	cmd = Command("df -h %s | tail -n 1" % volume)
	data = ([m.groupdict() for m in partition_regex.finditer(cmd.stdout)])[0]
	assert volume == data.get('mounted')
	assert float(size) <= float(data.get('size'))
	assert unit == data.get('unit')

@pytest.mark.parametrize("type, size, unit",[
	("Swap","32","G"),
	("Mem","16","G")
	])
def test_memory(Command, type, size, unit):
	cmd = Command("free -h %s | grep %s" % type)
	data = ([m.groupdict() for m in mem_regex.finditer(cmd.stdout)])[0]
	assert float(volume) <= float(data.get('total'))

def test_cpu(Command):
	cmd = Command("nproc")
	assert int(cmd.stdout) >= 4
