# Utilisation d'Ansible

Afin de pouvoir utiliser il faut surrement copier et saisir le mdp du compte "securise" delivre par AGEAS reseau : `cp passwd.sample passwd && vim passwd`.

## Execution de commandes one-shoot
* Ping tout les serveurs
```
ansible -i hosts all -m ping
```

* Tester si les serveurs sont à l'heure
```
ansible -i hosts docker -m shell -a 'date'
```

* Tester le device des nouveaux serveur à renseigner pour les fichiers de type : playbook\host_vars\sicav-xxx
```
ansible -i hosts new -m shell -a 'df -h|grep data'
```

* Tester que les serveurs fournis n'ont pas d'instance gnomes en cours d'execution
```
ansible -i hosts new -m shell -a 'sudo ps -A |grep gnom'
```

* Execute les tests d'infrastructure

Si besoin d'installer testinfra : `sudo pip install testinfra`
```
testinfra -v --connection=ansible --ansible-inventory ./hosts --hosts=vostok tests/*
```

* Tester la version de docker installée sur les serveurs
```
ansible -i hosts new -m shell -a 'docker version'
```

* Tester si tous les serveurs on bien un /var/lib/docker non surcharger
```
ansible -i hosts all -m shell -a "df -h|grep docker|| (echo 'no result : ' && exit 1)"
```

* Check les containers de tous les serveurs web disposant d'une instance oracle
```
ansible -i hosts bd -m shell -a 'docker ps -a -f "name=oracle"'
```

## Execution de scripts ansible
* Provisionner le serveur de developpement
```
ansible-playbook -i hosts \
--limit sicav-dev1 \
webservers.yml
```

* Provisionner le serveur de pré-production et production
```
ansible-playbook -i hosts \
--limit sicav-preprod1,sicav-web4 \
webservers.yml
```

* Tester sans provisionner le serveur

/!\ Ne marche pas toujours jusqu'au bout. Car certaines tâche son dépendantes les unes des autres
```
ansible-playbook -i hosts \
--limit sicav-preprod1 \
webservers.yml -C
```

* Tester que les serveurs fournis pour savoir si les pré-requis sont présent
```
ansible-playbook -i hosts \
--limit sicav-web5 \
prerequisiteServers.yml \
--extra-vars "ansible_sudo_pass=$(cat passwd)"
```

* Joue l'installation du serveur sicav-preprod3
```
ansible-playbook -i hosts \
--limit sicav-preprod3 \
webservers.yml \
--extra-vars "ansible_sudo_pass=$(cat passwd)"
```

* Mettre à jour la version de docker de x.xx.xx (ex: 1.12.6) à 18.03.1-ce
```
ansible-playbook -i hosts \
--limit sicav-preprod3 \
updateDocker.yml
```

* Lister les taches d'un playbook
```
ansible-playbook -i hosts \
--list-tasks \
webservers.yml
```

* Executer un playbook à partir d'une tâche donnée
```
ansible-playbook -i hosts \
--limit sicav-web5 \
--extra-vars "ansible_sudo_pass=$(cat passwd)" \
--start-at-task="Docker alias" \
updateDocker.yml
```

* Tester la version de docker installée
```
ansible -i hosts new -m shell -a 'docker --version'
```

## Installer un role de la communautée

```
cd playbook
ansible-galaxy install --roles-path ./roles/ geerlingguy.docker
```
