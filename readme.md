[![Build Status](http://jenkins-sicav/buildStatus/icon?job=deploy-pipeline)](http://jenkins-sicav/job/deploy-pipeline)

# Deploy
Ce projet à pour but de gérer le déploiement des projets sicavonline

## Intégration continu et Déploiement continu (CI/CD)
Ce projet est le premier à exploiter pleinement ce principe.
Il utilise [jenkins](http://jenkins-sicav/) pour tester automatiquement (CI : Continuous integration) et lors d'un push sur master déployer automatiquement la derniere version du projet si les tests sont OK (CD : continuous deployment).

Gitlab est paramétré pour envoyer un webhook au job 'deploy-pipeline' de jenkins [![Build Status](http://jenkins-sicav/buildStatus/icon?job=deploy-pipeline)](http://jenkins-sicav/job/deploy-pipeline)
'deploy-pipeline' déclenche 2 jobs jenkins qui s'enchaine si tout est OK :
- 'deploy-develop' est le job CI qui test que deployDev.sh fonctionne [![Build Status](http://jenkins-sicav/buildStatus/icon?job=deploy-develop)](http://jenkins-sicav/job/deploy-develop)
- 'deploy-deploiement-srvdev' est le job CD qui déploit la derniere version du projet deploy sur srvdev [![Build Status](http://jenkins-sicav/buildStatus/icon?job=deploy-deploiement-srvdev)](http://jenkins-sicav/job/deploy-deploiement-srvdev)
 
A savoir que les job de CI sont effectués grace à des tests éxécutés sur un system "fictif" créé à base d'un contenaire Docker :whale:. Dans ce container est joué un test avec l'outil [goss](https://github.com/aelsabbahy/goss) , pour tester les containers docker il faut utiliser le wrapper dgoss de ce même projet.
- Le contenaire est créé grace à ce [Dockerfile](Dockerfile)
- Les tests sont enregistrés dans ce fichier [goss.yaml](tests/assets/goss.yaml), si l'on souhaite ajouter des tests il faut utiliser cette commande :

    ```
    vagrant@DEV-VM ~/projects/deploy/tests/assets (git)-[develop] % dgoss edit --rm -it registry.intranet.ageas.fr:5000/deploy-test:deploy-test-sicav bash
    INFO: Starting docker container
    INFO: Container ID: 3c10c34d
    INFO: Run goss add/autoadd to add resources
    ...
    sh-4.2$ exit
    INFO: Deleting container
    130 vagrant@DEV-VM ~/projects/deploy/tests/assets (git)-[develop] % 
    ```

    Cette commande permet de teste la derniere image stable généré par le job 'deploy-develop'

    ```
    vagrant@DEV-VM ~/projects/deploy (git)-[develop] % docker run --rm registry.intranet.ageas.fr:5000/deploy-test:deploy-test-sicav goss v    :(
    ............................................................................................
    
    Total Duration: 0.002s
    Count: 92, Failed: 0, Skipped: 0
    ```

# La différence entre certificats
[source](https://www.sslmarket.fr/ssl/help-la-difference-entre-certificats)

Quelles sont les différences entre les certificats .p7b, .pfx, .p12, .pem, .der, .crt & .cer?

## Description
Avec tant de serveurs, certains nécessitent des formats différents.

## PEM Format
C'est le format le plus commun utilisé pour les certificats La plupart des serveurs (Ex: Apache) s'attendent à ce que les certificats et la clé privée se trouvent dans des fichiers séparés

Génerallement, ils sont des fichiers ASCII codés Base64
Les extensions utilisées pour les certificats PEM sont .cer, .crt, .pem, .key files
Apache et un serveur similaire utilisent des certificats de format PEM

## DER Format
Le format DER est la forme binaire du Certificat.Tous les types de certificats et clés privées peuvent être encodés au format DERLes certificats au format DER ne contiennent pas les déclarations "BEGIN CERTIFICATE / END CERTIFICATE"

Les certificats au format DER utilisent le plus souvent les extensions '.cer' et '.der'
DER est généralement utilisé dans les plates-formes Java

## P7B/PKCS#7 Format
Le format PKCS # 7 ou P7B est stocké au format Base64 ASCII et possède une extension de fichier de .p7b ou .p7c

Un fichier P7B ne contient que des certificats et des certificats de chaîne (CA intermédiaires), et non la clé privée
Les plates-formes les plus courantes qui prennent en charge les fichiers P7B sont Microsoft Windows et Java Tomcat

## PFX/P12/PKCS#12 Format
Le format PKCS#12 ou PFX/P12 est un format binaire pour stocker le certificat du serveur, les certificats intermédiaires et la clé privée dans un fichier chiffré.

Ces fichiers ont généralement des extensions telles que .pfx et .p12
Ils sont généralement utilisés sur les machines Windows pour importer et exporter des certificats et des clés privées
Si votre serveur / appareil nécessite un format de certificat différent autre que Base64 codé X.509, un outil tiers tel que OpenSSL peut être utilisé pour convertir le certificat au format approprié.

# Contenu du projet

## hooks/
Ici vous trouverez les hooks du serveur srvdev, en autre le post-receive qui gere la mise à jour des sites (ex: dev-back, ...) et la copie des projets sur le serveur gitlab.

## playbook/
![Ansible](https://hacksoft.io/media/images/ansible-logo.max-100x100.png)
[Comment lancer les scripts ansible](playbook/README.md)
http://doc-sicav/projects/deploy/documentation/docs/content/Ansible/index.html


## Config apache Pour tous les navigateurs 
Ici vous trouverer le lien pour plus d'information https://whatsmychaincert.com/
Il faut générer un fichier  combiner de .crt en utilisant cette structure  "example.com.chained.crt" à l'aide de la commande :
```shell 
cat example.com.crt example.com.chain.crt > example.com.chained.crt
```
```shell
la structure des délarations  SSL pour serveur Apache : 
▶/usr/local/httpd/conf/extra/httpd-vhosts.conf

SSLEngine on
SSLCertificateKeyFile /path/to/example.com.key
SSLCertificateFile /path/to/example.com.crt
SSLCertificateChainFile /path/to/example.com.chain.crt
```
## Autres
```shell

├── certificats
├── certificatsSites2
├── certificatsSites2.sh
├── certificatsSites.sh : Genere les CSR demandes pour les domaines
├── cert.sh
├── checkGitConfig.sh
├── checkParams.sh : Vérif des param de connexion à la BDD pour backoffice v1 et v2, core, tools et php_commun
├── coverage
├── createWorkspace.sh
├── cron-renew-cert-passwd
├── cron-renew-cert-passwd.sample
├── cron-renew-cert.sh
├── deployDev.sh : Script utilisé pour déployer les projets sous srvdev
├── deployHook.sh : Script utilisé pour déployer les hooks sous les projets git encore hebergés sous srvdev
├── deployProd.sh : SCript utilisé pour déployer les projets sous sicav-web0
├── Dockerfile
├── documentation
├── generationVhosts.php : Script utilisé pour générer les vhosts apache pour srvdev
├── gitNewRepo.sh
├── hooks
├── import_dump.sh
├── Makefile
├── nginxReboot.sh
├── parfile_struct_METADATA.par
├── parfile_table_parametrage.par : fichier utilise par le makefile dans le cas d un export/import sur un perimetre restreint
├── passwd
├── passwd2
├── playbook
├── rancher
├── readme.md
├── refreshSymbolicLink.sh
├── saveGitProjects.php
├── tests
├── testUpdateRancherPlayWithDocker.sh
└── transfertGit.sh
```