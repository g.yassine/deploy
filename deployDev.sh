#!/bin/bash

# ------------------------------------------------
# Initialisation de variables
# ------------------------------------------------
gitServer="gitlab-sicav"
gitUser="git"
apacheUser="nobody"
# gitBranch="feature/modif_build_mxl"
gitBranch="master"
gitConfig="--depth 1 --single-branch --config core.autocrlf=false"

nbBackup=4

sites_dir="/home/httpd"
releases_dir="/home/app/deploy"

tempColdfusion_dir="/usr/local/coldfusion/wwwroot/WEB-INF/cfclasses"

groupFile="info"

bin_dir="/home/app/bin"
php_bin="/usr/local/php-5.6.10/bin/php"
composer_bin="/home/app/bin/composer.phar"

destinatairesMail="s.salles@sicavonline.fr s.bouget@sicavonline.fr x.hermin@sicavonline.fr g.moulin@sicavonline.fr" # adresses séparées par des espaces

current=$(date +%Y%m%d%H%M%S)

# ------------------------------------------------
# Fonctions
# ------------------------------------------------
function clone {
  case $projet in
      "trash"|"documentation"|"Dockerfile"|"example"|"tpSymfony"|"droits"|"vagrant"|"deploy" ) prefixProject="it-sol/" ;;
      "agp"|"studio_cfm"|"nc2"|"avenir"|"scpi"|"adyal"|"tmptrash"|"studio"|"portail"|"part"|"exemple"|"1688"|"ariane"|"core"|"CoreBundle"|"test" ) prefixProject="sicav/" ;;
      *) prefixProject="" ;;
  esac

  # Fix pour le temps où l'on à encore le serveur git de srvdev fonctionnel pour ces 4 derniers projets
  case $projet in
      "backoffice"|"bo"|"tools"|"php_commun" )
        gitServer="srvdev"
	prefixProject="/home/git/"
      ;;
  esac

  # pour alléger le clone ne prendre que la derniere version
  commandeGit="git clone -b ${gitBranch} ${gitConfig} ${gitUser}@${gitServer}:${prefixProject}${projet}.git ${releases_dir}/${current}"

  if [ ! -f $releases_dir ]; then
    mkdir -p $releases_dir
  fi

  if [ $mute = false ]; then
    echo -e " Clone du projet '\033[32m$projet\033[0m':"
    echo "=============================="
    echo $commandeGit
  fi

  $($commandeGit)

  sudo chown $USER:$groupFile -R $releases_dir/$current
  if [ $mute = false ]; then
    echo -e " * \033[32mDone.\033[0m"
  fi

  phing
  restoreConfig
}

function creationLienSymbolique {
  commandeSuppressionLn="rm $sites_dir/$projet"
  commmandeCreationLn="ln -s $1 $sites_dir/$projet"

  if [ $mute = false ]; then
    echo " Création du lien symbolique"
    echo "============================"
    echo $commandeSuppressionLn
    echo $commmandeCreationLn
  fi

  rm $sites_dir/$projet
  ln -s $1 $sites_dir/$projet
#  sudo chown $USER:$groupFile -h $sites_dir/$projet
  chown $USER:$groupFile -h $sites_dir/$projet

  if [ $mute = false ]; then
    echo -e " * \033[32mDone.\033[0m"
  fi
}

function suppressionAncieneVersion {
  nbLigne=$(($nbBackup+1))

  ls $releases_dir | sort -r | tail -n +$nbLigne | xargs -I{} -r sudo rm -rf $releases_dir/{};

  if [ $mute = false ]; then
    echo " Les anciennes releases ont été supprimées :"
    echo "============================================"
    # ls -l $releases_dir
    echo -e " * \033[32mDone.\033[0m"
  fi
}

function phing {
  if [ $mute = false ]; then
    echo " Initialisation du projet :"
    echo "=========================="
    optPhing=""
  else
    optPhing="-S"
  fi
  if [ -f $releases_dir/$current/config/build.xml ]; then
    $php_bin $bin_dir/phing.phar $optPhing -f $releases_dir/$current/config/build.xml

    if [ $mute = false ]; then
      echo -e " * \033[32mDone.\033[0m"
    fi
  else
    if [ $mute = false ]; then
      echo " * Pas de fichier : $releases_dir/$current/config/build.xml"
    fi
  fi
}

# Copie les fichiers de config du projet actuellement utilisé dans le projet que l'on vient de cloner
function restoreConfig {
  if [ $mute = false ]; then
    echo " Copie des fichiers de config :"
    echo "==============================="
  fi
    recuperationFichiers=false
    message=""

    fichiersParametre=(
        "config/parameters.cfm"
        "config/parameters.php"
        "app/config/parameters.yml"
        )

    for fichierParam in ${fichiersParametre[@]}; do
        if [ -f "$oldLien/$fichierParam" ]; then
            recuperationFichiers=true
            sudo cp -p $oldLien/$fichierParam $releases_dir/$current/$fichierParam
            sudo chown $USER:$groupFile $releases_dir/$current/$fichierParam

            if [ $mute = false ]; then
              message+=" * copie de $fichierParam\n" true
            fi

        fi
    done
    if [[ $mute = false && $recuperationFichiers = true ]]; then
        echo "Recupétation des fichiers de configuration :\n$message"
    fi
}

function rollback {
  if [ $mute = false ]; then
    echo " Retour sur la version précédente :"
    echo "==================================="
    echo " L'ancien lien etait : $oldLien"
  fi
  rm $sites_dir/$projet
  ls $releases_dir | tail -n 2 | head -n 1 | xargs -I{} -r ln -s $releases_dir/{} $sites_dir/$projet;
  if [ $mute = false ]; then
    echo " Le lien actuel est : $(ls -lh $sites_dir/$projet | awk '{print $11}')"
    echo " Les anciennes releases sont :"
  fi
  ls -l $releases_dir
  if [ $mute = false ]; then
    echo -e " * \033[32mDone.\033[0m"
  fi
}

function videCacheColdFusion {
  fileCheckSymfony="$releases_dir/$current/app/AppKernel.php"
  if [ -f "$fileCheckSymfony" ]; then
    echo ""
  else
    if [ $mute = false ]; then
      echo " Redemarrage du serveur Coldfusion :"
      echo "===================================="
    fi
    sudo /etc/rc.d/init.d/coldfusion stop
    cd $tempColdfusion_dir
    sudo rm -f *
    cd -
    sudo /etc/rc.d/init.d/coldfusion start
    if [ $mute = false ]; then
      echo -e " * \033[32mDone.\033[0m"
    else
      echo -e " Redemarrage du serveur Coldfusion : \033[32mDone.\033[0m"
    fi
  fi
}

# Vérifie si le projet est un projet Symfony et exécute le procéssus de déploiement nécessaire
function siProjetSymfonyInstallation {
    installSymfony=false
    fileCheckSymfony="$releases_dir/$current/app/AppKernel.php"
    if [ -f $fileCheckSymfony ]; then
        if [ $mute = false ]; then
            echo -e "le projet ayant un '$fileCheckSymfony' c'est un projet symfony -> composer install"
        fi
        cd "$releases_dir/$current"
        installSymfony=true
    fi
    
    fileCheckSymfony="$releases_dir/$current/v2/app/AppKernel.php"
    if [ -f $fileCheckSymfony ]; then
        if [ $mute = false ]; then
            echo -e "le projet ayant un '$fileCheckSymfony' c'est un projet symfony -> composer install"
        fi
        cd "$releases_dir/$current/v2"
        installSymfony=true
    fi
  
  if [ $installSymfony == true ]; then
    # MEP d'un site symfony : http://symfony.com/doc/current/cookbook/deployment/tools.html
    if [ $mute = false ]; then
      echo " Projet Symfony ?"
      echo "================="
      echo "le projet ayant un fichier '$fileCheckSymfony' c'est un projet symfony -> composer install"
      echo "remplacement du path du core"
    fi
    if [ $mute = false ]; then
      echo "Suppression de app/cache"
    fi
    if [ -f app/cache ]; then
      chmod 0777 -R app/cache
    fi
    if [ -f var/cache ]; then
      chmod 0777 -R var/cache
      chmod 0777 -R var
      
      sudo chown $apacheUser: -R var/sessions
    fi

    if [ $mute = false ]; then
      echo "Installation des vendors (en tache de fond)"
    fi
    # les caches de prod ne sont pas générés la premiere fois, il faut se connecter une premiere fois
    $php_bin $composer_bin install --no-interaction

    # nettoyage du code pour l'utilisateur $apacheUser
    rm web/app_dev.php
    if [ -f var/cache ]; then
      sudo chown $apacheUser var/logs/*
      sudo chown $apacheUser -R var/cache/*
    fi
    if [ -f app/cache ]; then
      sudo chown $apacheUser app/logs/*
      sudo chown $apacheUser -R app/cache/*
    fi

    cd -
  fi
}

function droitsOldProject {
  if [ $mute = false ]; then
    echo " Application des droits correct sur l'ancien projet :"
    echo "====================================================="
    echo " tout le dossier '$oldLien' change d'utilisateur pour '$USER:$groupFile'"
  fi
  sudo chown $USER:$groupFile -R $oldLien
}

function notifmail {
    if [ "$1" = "rollback" ]; then
      dirSave="$sites_dir/$projet"
      echo -e "Rollback sur $HOSTNAME de '\033[32m$projet\033[0m' ($dirSave) :"
      echo "$projet : rollback $dirSave" | mail -s "$projet : Rollback sur $HOSTNAME à la version $dirSave \nle : $(date)" $destinatairesMail
    else
      dirSave="$releases_dir/$current"
      echo -e "Déploiement sur $HOSTNAME de '\033[32m$projet\033[0m' ($dirSave) :"
      echo "$projet : déploiement $dirSave" | mail -s "$projet : Déploiement sur $HOSTNAME à la version $dirSave \nle : $(date)" $destinatairesMail
    fi
}
# ------------------------------------------------
# Main
# ------------------------------------------------
if [ $(whoami) != "git" ]; then
  echo "Vous devez executer ce script en tant que l'utilisateur 'git'."
  exit 1
fi

if [ $(sudo whoami) != "root" ]; then
  echo "Votre utilisateur '$(whoami)' n'a pas les droits sudo."
  exit 1
fi

# si le parametre 1 est --help alors affiche l'aide
if [ "$1" = "--help" ] || [ "$1" = "-h" ] || [ "$#" -gt 3 ]; then
    if [ "$#" -gt 3 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
    deployDev.sh
    ============

    Ce script permet de déployer les projets git sur srvdev.
    Ce script doit être utilsé par l'utilisateur git.

    Exemple d'utilisation :

    $ ./deployDev.sh [nom du projet git] [branche à deployer | rollback] [mute]
EOF
  exit 0
fi

# Utilise le parametre 1 comme projet à deployer, s'il n'est pas transmit alors utilise le projet exemple
if [ "$1" != "" ]; then
  projet="$1"
else
  projet="exemple"
fi

# si le parametre 3 est mute alors pas beaucoup de retour affiché pour par exemple l'automatisation lors d'un push
if [ "$3" = "mute" ]; then
  mute=true
else
  mute=false
fi

releases_dir="$releases_dir/$projet/releases"
oldLien=$(ls -l $sites_dir/$projet | awk '{print $11}')

# si le parametre 2 est rollback alors on execute ça sinon c'est le parametre de branche
if [ "$2" = "rollback" ]; then
  rollback
  notifmail $2
  exit 0
elif [ "$2" != "" ]; then
  gitBranch="$2"
fi

# restaure les droits sur le projet actuellement actif pour être sûr de ne pas rencontrer de pb lors des copies
droitsOldProject
clone
siProjetSymfonyInstallation
creationLienSymbolique "$releases_dir/$current"
# Mode 777 pour régler simplement des problèmes applicatifs dû aux changement de structure, TODO à supprimer aprés amélioration
sudo chmod -R 777 $releases_dir/$current
suppressionAncieneVersion
# videCacheColdFusion

if [ $mute = true ]; then
  echo -e "Le projet \"\033[32m$projet\033[0m\" (branch: $gitBranch) vient d'être automatiquement déployé sur '$sites_dir/$projet' le lien symbolique pointe sur '$releases_dir/$current'"
fi
notifmail

exit 0
