#!/usr/bin/env bash
# Bash3 Boilerplate. Copyright (c) 2014, kvz.io
# http://kvz.io/blog/2013/11/21/bash-best-practices/
set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace
# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app
arg1="${1:-}"

CHEMIN="${__dir}/certificats"
CHEMIN_DEST="${__dir}/certificats/generate"

SRC_CA_CERT="${CHEMIN}/STAR_wishizz_fr.pfx"

CA_PEM="${__dir}/AddTrustExternalCARoot.crt"

NOM_CERT="STAR_wishizz_fr"
PASSWD=$(cat passwd2)
TEMPORARY_PASSWD="TemporaryPassword"

if [ ! -d $CHEMIN_DEST ]; then
    mkdir -p $CHEMIN_DEST
fi

# Generation des fichiers OK pour prise en charge par nginx
if [ ! -f ${CHEMIN_DEST}/${NOM_CERT}.key ]; then
    openssl pkcs12 \
    -in ${SRC_CA_CERT} \
    -nocerts \
    -password pass:${PASSWD} \
    -nodes \
    -out ${CHEMIN_DEST}/${NOM_CERT}.key # pfx to key
fi

# Generation des fichiers OK pour prise en charge par nginx
if [ ! -f ${CHEMIN_DEST}/${NOM_CERT}.crt ]; then
    openssl pkcs12 \
    -in ${SRC_CA_CERT} \
    -clcerts \
    -password pass:${PASSWD} \
    -nokeys \
    -out ${CHEMIN_DEST}/${NOM_CERT}.crt # pfx to crt
fi
