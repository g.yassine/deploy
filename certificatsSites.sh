#!/usr/bin/env bash

set -o pipefail
set -o errtrace
set -o nounset
set -o errexit
# to trace what gets executed.
# set -o xtrace

ARG=${1:-}

SITES=(
    "backoffice.sicavonline.fr"
    "bo.sicavonline.fr"
    "fip-fcpi.sicavonline.fr"
    "scpi.sicavonline.fr"
    "souscription.sicavonline.fr"
    "studio.sicavonline.fr"
    "www.sicavonline.fr"
    "www.ageas-patrimoine.fr"
    "souscription.ageas-patrimoine.fr"
    "*.advenis.com"
    "www.motion-way.fr"
    "STAR.sicavonline.fr"
)

__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PATH_HTTPD="/media/sicav-nas1/config/httpd/"
PATH_HTTPD_CRT="${PATH_HTTPD}ssl.crt/"
PATH_HTTPD_CSR="${PATH_HTTPD}ssl.csr/"
PATH_HTTPD_KEY="${PATH_HTTPD}ssl.key/"
PATH_TMP_CORE_MAIL="/tmp/coreMailCertificatsSites.txt"
PATH_TAR="/tmp/archive-certificats-sites.tar"
PATH_CERT_SSL_CA_CERTIFICATE_FILE="/usr/local/httpd/conf/ssl.crt/cert_Geotrust_rsa_2018.crt"
DATE=`date +"%Y"`
PASSWORD=`cat ${__dir}/cron-renew-cert-passwd` # /root/.acme.sh/cron-renew-cert-passwd sur sicav-web2
MAIL_FROM="certificatsSites.sh <$(id -u)@$(uname -n)>"
MAIL_DEST=s.salles@sicavonline.fr

# si le parametre 1 est --help alors affiche l'aide
if [ "${ARG}" = "--help" ] || [ "${ARG}" = "-h" ] || [ "$#" -ge 1 ]; then
    if [ "$#" -ge 1 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
    certificatsSites.sh
    ===================
    Generation des fichier key et csr pour tous les SITES saisis.
    Si ces fichiers existent déjà ils ne seront pas générés de nouveau.

    Pré-requis : sudo mkdir -p $PATH_HTTPD{ssl.key,ssl.csr,ssl.key}
EOF
    exit 0
fi

for SITE in ${SITES[@]}; do
    echo ""
    echo ">>> ${SITE}"
    echo ">>> Génération de la clé et du CSR [$(date)]"
    echo "\_ dans le path : ${PATH_HTTPD_KEY}${SITE}.key.${DATE}"
    if [ ! -e ${PATH_HTTPD_KEY}${SITE}.key.${DATE} ] && [ ! -e ${PATH_HTTPD_KEY}${SITE}.csr.${DATE} ]; then
        openssl req -new -newkey rsa:2048 -nodes -keyout ${PATH_HTTPD_KEY}${SITE}.key.${DATE} -out ${PATH_HTTPD_CSR}${SITE}.csr.${DATE}
        cp -f ${PATH_HTTPD_KEY}${SITE}.key.${DATE} ${PATH_HTTPD_KEY}${SITE}.key
        cp -f ${PATH_HTTPD_CSR}${SITE}.csr.${DATE} ${PATH_HTTPD_CSR}${SITE}.csr
        ls -lh ${PATH_HTTPD_KEY}${SITE}.key.${DATE} ${PATH_HTTPD_CSR}${SITE}.csr.${DATE} ${PATH_HTTPD_KEY}${SITE}.key ${PATH_HTTPD_CSR}${SITE}.csr || exit 0
    else
        echo "La clé et CSR existe déjà ils ne sont donc pas généré de nouveau"
        ls -lh ${PATH_HTTPD_KEY}${SITE}.key.${DATE} ${PATH_HTTPD_CSR}${SITE}.csr.${DATE} || exit 0

        if [ -e ${PATH_HTTPD_CRT}${SITE}.cer ] && [ -e ${PATH_CERT_SSL_CA_CERTIFICATE_FILE} ] && [ -e ${PATH_HTTPD_KEY}${SITE}.key ] ; then
            echo ">>> Génération du pfx ${PATH_HTTPD_CRT}${SITE}.pfx [$(date)]"
            openssl pkcs12 -in ${PATH_HTTPD_CRT}${SITE}.cer -certfile ${PATH_CERT_SSL_CA_CERTIFICATE_FILE} -inkey ${PATH_HTTPD_KEY}${SITE}.key -password pass:${PASSWORD} -export -out ${PATH_HTTPD_CRT}${SITE}.pfx
        fi
    fi
    echo ">>> Ajout du ${PATH_HTTPD_CSR}${SITE}.csr [$(date)]"
    tar -rvf ${PATH_TAR} ${PATH_HTTPD_CSR}${SITE}.csr
done

# PATH_CERT_SSL_CA_CERTIFICATE_FILE
echo ">>> Envoi des certificats par mail [$(date)]"
echo "Vous trouverez ci joint les cerficats générés ce jour." > ${PATH_TMP_CORE_MAIL}
echo "" >> ${PATH_TMP_CORE_MAIL}
tar -rvf ${PATH_TAR} >> ${PATH_TMP_CORE_MAIL}
cat ${PATH_TMP_CORE_MAIL} | EMAIL=${MAIL_FROM} mutt -a "${PATH_TAR}" -s "Certificats du $(uname -n) le $(date)" -- ${MAIL_DEST-s.salles@sicavonline.fr}
rm ${PATH_TMP_CORE_MAIL} ${PATH_TAR}

echo ""
echo ">>> Paramétrage d’Apache : [$(date)]"
cat << EOF
    Les directives suivantes (peuvent être de niveau <VirtualHost>) permettent l’utilisation de la bonne clé et du bon certificat :

    SSLCertificateFile /usr/local/httpd/conf/ssl.crt/expa.crt
    SSLCertificateKeyFile /usr/local/httpd/conf/ssl.key/expa.key

    Il faut alors demader à Apache de prendre ces nouveaux paramètres en compte :

    /etc/rc.d/init.d/httpd graceful
EOF

echo ">>> Fin du traitement [$(date)]"
exit 1
