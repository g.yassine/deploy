#!/bin/sh

if [ $(sudo whoami) != "root" ]; then
  echo "Votre utilisateur '$(whoami)' n'a pas les droits sudo."
  exit 1
fi

# pour l'installer faire :
# sudo ln -s /home/httpd/ss/deploy/gitNewRepo.sh /usr/bin/gitNewRepo

echo ""
repo="/home/git/$1.git"
sudo mkdir $repo
cd $repo
sudo git init --bare
sudo chown git:git -R $repo
echo "==="
echo "Repository GIT créé"
echo "git remote add origin ssh://git@srvdev$repo"
