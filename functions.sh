#
# Affiche est log dans $dirLogs le message passé en paramétre $1 et ajoute un flag ok ou ko en fonction du paramétre $2
#
function echoLog {
    # determine de préfixe selon le paramétre 2
    case ${2:-duck} in
        true )
            prefixe="[\033[32mOk\033[0m] "
        ;;
        false )
            prefixe="[\033[31mKo\033[0m] "
        ;;
        "alert" )
            prefixe="\033[31m/!\ \033[0m"
        ;;
        "duck" )
            prefixe=""
            echo -e " .__(.)< ( ${1:-coin} )"
            echo "  \___)"
        ;;
        *)
            prefixe=""
        ;;
    esac

    lineLog="$prefixe$1"

    if [ ! -f $dirLogs ]
    then
        mkdir -p $dirLogs
    fi
    echo -e "$(date) $lineLog" >> $dirLogs/deploy-prod-$projet.log
    echo -e "$(date) $lineLog"
}

function deleteDossierSav {
    suppressionSauvegarde=true
    rm -rf $dirSave

    echoLog "Suppression du dossier de sauvegarde" true
}

function createDossierSavBackOffice {
    mkdir -p $dirSave/img
    mkdir -p $dirSave/pages/presse
    mkdir -p $dirSave/pages/contacts
    mkdir -p $dirSave/pages/voeux

    echoLog "Création du dossier de sauvegarde pour préserver les dossiers de Backoffice" true
}

function createDossierSavPhpCommun {
    mkdir -p $dirSave

    echoLog "Création du dossier de sauvegarde pour préserver les dossiers de PhpCommun" true
}

function saveDossiersBackOffice {
    createDossierSavBackOffice
    echoLog "Debug de biblio (save)" alert

    for file in ${backupFiles[@]}; do
        if [ -f "$dirProd/$projet/$file" ]; then
            echoLog "$dirProd/backoffice/$file $dirSave/$file" true
            mv $dirProd/backoffice/$file $dirSave/$file
        fi
    done

    echoLog "Sauvegarde des dossiers du backoffice" true
}

function restoreDossiersBackoffice {
    echoLog "Debug de biblio (restore)" alert
    for file in ${backupFiles[@]}; do
        if [ -f "$dirProd/$projet/$file" ]; then
            echoLog "Deplacement de : $dirSave/$file $dirProd/backoffice/$file" true
            mv $dirSave/$file $dirProd/backoffice/$file
        fi
    done

    echoLog "Restauration des dossiers" true
}

#
# Ancienne function qui à été remplacé par la logique de recupereDerniereVersionProjet plus rapide avec le transfert entre serveur
#
function cloneProjet {
    case $projet in
        "trash"|"documentation"|"Dockerfile"|"example"|"tpSymfony"|"tools"|"droits"|"vagrant"|"deploy" ) prefixProject="it-sol/" ;;
        "studio_cfm"|"nc2"|"avenir"|"scpi"|"php_commun"|"adyal"|"tmptrash"|"studio"|"portail"|"part"|"exemple"|"bo"|"backoffice"|"1688"|"ariane"|"core"|"CoreBundle"|"test" ) prefixProject="sicav/" ;;
        *) prefixProject="" ;;
    esac

    # Fix pour le temps où l'on à encore le serveur git de srvdev fonctionnel pour ces 4 derniers projets
    case $projet in
        "backoffice"|"bo"|"tools"|"php_commun" )
            gitServer="srvdev"
            prefixProject="/home/git/"
        ;;
    esac

    # pour alleger le clone ne prendre que la derniere version
    # TODO le projet tools aura un soucis à cause du :sicav/
    commandeGit="git clone -b ${gitBranch} ${gitConfig} ${gitUser}@${gitServer}:${prefixProject}${projet}.git ${dirSave}/clone"

    if [ ! -f $dirSave/clone ]
    then
        mkdir -p $dirSave/clone
    fi

    echoLog "Clone du projet '\033[32m$projet\033[0m' ($gitBranch) :\n$commandeGit"

    $($commandeGit)
    if [ $serveurDev == true ] ; then
        sudo chown :$groupFile -R $dirSave/clone
    fi
    echoLog "Clone effectué" true
}

function recupereDerniereVersionProjet {
    exclusionsFile="/tmp/deploy-prod-exclusions-$projet"
    fichierBuildXml="$dirProd/$projet/config/build.xml"

    # On recupere dans le dossier $dirSave/clone la derniere version du projet
    if [ ! -f $dirSave/clone ] ; then
        mkdir -p $dirSave/clone
    fi

    if [ -f "$fichierBuildXml" ]; then
        grep -n 'property name="paths_to_exclude"' $fichierBuildXml | awk '{print substr($4, 8, length($4)-8)}' | tr "," "\n" > $exclusionsFile
        exclude="--exclude-from=$exclusionsFile"
    else
        exclude=""
    fi

    echoLog "Recupération de la derniere version de production du projet '\033[32m$projet\033[0m' ici : $dirSave/clone"
    sudo rsync -prt $exclude $dirProd/$projet/ $dirSave/clone
    echoLog "Done." true

    if [ $serveurDev == true ] ; then
        # à decommenter pour des tests sur le serveur de dev, mais à ne pas utiliser en production
        sudo chown $USER -R $dirSave/clone
    fi
    cd $dirSave/clone
    git fetch
    git checkout $gitBranch
    git stash -u
    git reset --hard HEAD
    git rebase
    git stash drop
    createTagMep

    cd -
    echoLog "MaJ de cette nouvelle version" true
}

function createTagMep {
    derniereVersionDuMois="$(git tag -l "$(date +%y.%m.)*" | tail -n 1 | awk '{print substr($0, 7)}')"
    derniereVersionDuMois=$(($derniereVersionDuMois+1))
    nouveauTag="$(date +%y.%m.$derniereVersionDuMois)"

    git tag $nouveauTag
    echoLog "Nouveau tag créé : $nouveauTag"
    # Envoi les tag sur le remote
    git push --tags
}

# Déplace le projet actuellement en production dans le dossier release pour en faire une sauvegarde
# et déplace le projet cloné en production
function mepNewVersion {
    mkdir -p $dirReleaseIni/$projet
    mv $dirProd/$projet $dirReleaseIni/$projet/$current
    mv $dirSave/clone $dirProd/$projet

    echoLog "Nouvelle version de '\033[32m$projet\033[0m' mise en production ici : '$dirProd/$projet'" true
    echoLog "Ancienne version sauvegardé ici : '$dirReleaseIni/$projet/$current'"
}

function rollback {
    ancienRelease=$(ls $dirReleaseIni/$projet | tail -n 1)

    if [ ! -e $dirReleaseIni/$projet ]; then
        echoLog "Le Rollback ne peut pas se faire car le dossier '\033[31m$dirReleaseIni/$projet\033[0m' n'est pas trouvé"
        exit 1
    fi
    if [ $(ls $dirReleaseIni/$projet | wc -l) = 0 ]; then
        echoLog "Le Rollback ne peut pas se faire car le dossier '\033[31m$dirReleaseIni/$projet\033[0m' ne contient pas de release"
        exit 1
    fi

    echoLog "Retour sur la version précédente '$ancienRelease' :"

    # Sauvegarde les dossiers si besoin
    case $projet in
        "backoffice" ) saveDossiersBackOffice;;
        "php_commun" ) saveDossiersPhpCommun;;
    esac
    # créé la release actuelle dans un dossier de rollback pour sauvegarder la prod en cours
    mkdir -p /home/httpd/sav/rollback/$projet
    echoLog "créé la release actuelle pour sauvegarder la prod en cours '\033[32m/home/httpd/sav/rollback/$projet/$current\033[0m'" true
    # deplace le projet en prod actuel dans le dossier de rollback
    mv $dirProd/$projet /home/httpd/sav/rollback/$projet/$current
    echoLog "deplace le projet en prod actuel dans le dossier de rollback" true
    # deplace l'ancien vers le nouveau
    mv $dirReleaseIni/$projet/$ancienRelease $dirProd/$projet
    echoLog "deplace l'ancien release vers la production" true
    # copie les dossiers vers l'actuel
    case $projet in
        "backoffice" ) restoreDossiersBackoffice
            checkDossiersBackOffice
        ;;
        "php_commun" ) restoreDossiersPhpCommun
            checkDossiersPhpCommun
        ;;
    esac
}

# Copie les fichiers de config du projet actuellement en production dans le projet que l'on vient de cloner
function recuperationFichiersConfig {
    recuperationFichiers=false
    message=""

    fichiersParametre=(
        "config/parameters.cfm"
        "config/parameters.php"
        "app/config/parameters.yml"
    )

    for fichierParam in ${fichiersParametre[@]}; do
        if [ -f "$dirProd/$projet/$fichierParam" ]; then
            recuperationFichiers=true
            cp $dirProd/$projet/$fichierParam $dirSave/clone/$fichierParam

            message+="      $fichierParam\n" true
        fi
    done

    if [ $recuperationFichiers = true ]; then
        echoLog "Recupétation des fichiers de configuration :\n$message"
    fi
}

function suppressionAncienneVersion {
    nbLigne=$(($nbBackup+1))

    ls $dirReleaseIni/$projet | sort -r | tail -n +$nbLigne | xargs -I{} -r sudo rm -rf $dirReleaseIni/$projet/{};
    # rm -r $dirSave

    echoLog "Les anciennes releases ont été supprimées, seulement $nbBackup sont conservées" true
    # ls -l $dirReleaseIni/$projet
    if [ $suppressionSauvegarde = false ] && [ $projet != "backoffice" ] && [ $projet != "php_commun" ]; then
        deleteDossierSav
    fi
}

function saveDossiersPhpCommun {
    createDossierSavPhpCommun

    dossiersASauvegarder=(
        "excel"
        "jpgraph"
        "ExtJS4"
    )

    for dossier in ${dossiersASauvegarder[@]}; do
        mv $dirProd/php_commun/$dossier $dirSave
    done

    echoLog "Sauvegarde des dossiers de Php Commun" true
}

function restoreDossiersPhpCommun {
    dossiersARestaurer=(
        "ExtJS4"
        "excel"
        "jpgraph"
    )

    for dossier in ${dossiersARestaurer[@]}; do
        mv $dirSave/$dossier $dirProd/php_commun/$dossier
    done

    echoLog "Restauration des dossiers" true
}

function checkDossiersBackOffice {
    erreurCheck=false

    echoLog "Check des dossiers du Backoffice :"
    for dossier in ${backupFiles[@]}; do
        if [ -e "$dirProd/backoffice/$dossier" ]; then
            echoLog "$dirProd/backoffice/$dossier" true
        else
            erreurCheck=true
            echoLog "$dirProd/backoffice/$dossier" false
        fi
    done

    if [ $erreurCheck = false ]; then
        deleteDossierSav
    fi
}

function checkDossiersPhpCommun {
    erreurCheck=false
    dossiersACheck=(
        "$dirProd/php_commun/ExtJS4"
        "$dirProd/php_commun/excel"
        "$dirProd/php_commun/jpgraph"
    )

    echoLog "Check des dossiers du PhpCommun :"
    for dossier in ${dossiersACheck[@]}; do
        if [ -e "$dossier" ]; then
            echoLog "$dossier" true
        else
            erreurCheck=true
            echoLog "$dossier" false
        fi
    done

    if [ $erreurCheck = false ]; then
        deleteDossierSav
    fi
}

function notifmail {
    infoMail="\nCe mail a été envoyé depuis ${__file}.sh"
    if [ "${1:-none}" = "rollback" ]; then
        echoLog "Rollback en production de '\033[32m${projet}\033[0m' (${dirSave}) :"
        echo -e "Le projet ${projet} vient d'être rollback. ${infoMail}" | mail -s "${projet} : Rollback en production de la version ${nouveauTag}" ${destinatairesMail}
    else
        echoLog "Déploiement en production de '\033[32m${projet}\033[0m' (${dirSave}) :"
        echo -e "Le projet ${projet} vient d'être déployé. ${infoMail}" | mail -s "${projet} : Déploiement en production de la version ${nouveauTag}" ${destinatairesMail}
    fi
}

# Vérifie si le projet est un projet Symfony et exécute le procéssus de déploiement nécessaire
function siProjetSymfonyInstallation {
    installSymfony=false
    fileCheck="$dirProd/$projet/app/AppKernel.php"
    if [ -f $fileCheck ]; then
        echoLog "le projet ayant un '$fileCheck' c'est un projet symfony -> composer install"
        cd $dirProd/$projet
        installSymfony=true
    fi

    fileCheck="$dirProd/$projet/v2/app/AppKernel.php"
    if [ -f $fileCheck ]; then
        echoLog "le projet ayant un '$fileCheck' c'est un projet symfony -> composer install"
        cd "$dirProd/$projet/v2"
        installSymfony=true
    fi

    if [ $installSymfony == true ]; then
        # MEP d'un site symfony : http://symfony.com/doc/current/cookbook/deployment/tools.html
        composer install
        echoLog "Voulez-vous executer la mise à jour de la BD de façon recursive ? (y/l/n (oui/last/none))" alert
        read wantRecursif
        case $wantRecursif in
            "y" )
                php app/console core:migration:migrate --recursive
            ;;
            "l" )
                php app/console core:migration:migrate
                exit
            ;;
            *)
            ;;
        esac

        if [ -f app/cache ]; then
            chmod 0777 -R app/cache
        fi
        if [ -f var/cache ]; then
            chmod 0777 -R var/cache
            chmod 0777 -R var
        fi
        php app/console cache:clear --env=prod
        rm web/app_dev.php
        cd -
    fi
}
