#!/bin/bash

if [ "$1" = "--help" ] || [ "$1" = "-h" ] || [ "$#" -ne 3 ]; then
    if [ "$#" -ne 3 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
    createWorkspace.sh
    ==================

    Ce script créé le repertoire utilisateur dans le dossier '/home/httpd', y génére les projets necessaires en les positionnant sur la branche develop, y depose les droits de commit si existant et copie un .gitconfig pour l'utilisateur pour que l'utilisation de git en ssh soit confortable.

	/!\ Il est necessaire de consulter le fichier createWorkspace.sh pour comprendre son utilité avant de l'executer !

	userName 	: Nom de l'utilisateur
	groupName 	: Groupe de l'utilisateur
	userDir 	: Dossier de l'utilisateur

    Exemple d'utilisation :

    $ ./refreshSymbolicLink.sh [userName] [groupName] [userDir]

    e.g: ./refreshSymbolicLink.sh a.elkasmi a.elkasmi ae
    e.g: ./refreshSymbolicLink.sh sb info sb
EOF
    exit 0
fi

# ------------------------------------------------
# Variables initialisation
# ------------------------------------------------
userName=$1
groupName=$2
userDir=$3

workspaceModel="ss"
passwordDefault="soleil123"

# Liste des projets que l'on veux deployer, il suffit de décommenter les lignes
projects=(
    # "ssh://git@srvdev/home/git/tools.git"
    # "ssh://git@srvdev/home/git/bo.git"
    # "ssh://git@srvdev/home/git/backoffice.git"

    # "git@gitlab-sicav:it-sol/sicavRancherCatalog "
    # "git@gitlab-sicav:it-sol/documentation"
    # "git@gitlab-sicav:it-sol/Dockerfile"
    # "git@gitlab-sicav:it-sol/tools"
    # "git@gitlab-sicav:it-sol/droits"
    # "git@gitlab-sicav:it-sol/vagrant"
    # "git@gitlab-sicav:it-sol/deploy"
    # "git@gitlab-sicav:sicav/presentation"
    # "git@gitlab-sicav:sicav/aia"
    # "git@gitlab-sicav:sicav/agp"
    # "git@gitlab-sicav:sicav/backoffice"
    # "git@gitlab-sicav:sicav/baseApporteur"
    # "git@gitlab-sicav:sicav/gitflow"
    "git@gitlab-sicav:sicav/afin"
    # "git@gitlab-sicav:sicav/avenir"
    "git@gitlab-sicav:sicav/afim"
    # "git@gitlab-sicav:sicav/studio_cfm"
    "git@gitlab-sicav:sicav/nc2"
    "git@gitlab-sicav:sicav/scpi"
    "git@gitlab-sicav:sicav/php_commun"
    # "git@gitlab-sicav:sicav/adyal"
    "git@gitlab-sicav:sicav/studio"
    "git@gitlab-sicav:sicav/portail"
    # "git@gitlab-sicav:sicav/part"
    # "git@gitlab-sicav:sicav/bo"
    # "git@gitlab-sicav:sicav/1688"
    # "git@gitlab-sicav:sicav/ariane"
    "git@gitlab-sicav:sicav/core"
    # "git@gitlab-sicav:sicav/CoreBundle"
)

filesConfig=(
    "dirs_authorized"
    "files_authorized"
    "users_authorized"
)

lienSymbolique=(
    "images"
    "avenir"
    # "afin"
)

dirHttpd="/home/httpd"
destinationDirGitDroits="/home/git/droits"

modeleDirGitConfig="$dirHttpd/$workspaceModel/deploy/hooks/preCommit"
modeleGitConfig="/home/$workspaceModel/.gitconfig"

# ------------------------------------------------
# Fonctions
# ------------------------------------------------
genereDirDroitProjet() {
    repertoireDroitProjet="$destinationDirGitDroits/$1"
    if [ ! -e $repertoireDroitProjet ]; then
        sudo mkdir -p $repertoireDroitProjet
        sudo chown git:git -R $repertoireDroitProjet
    fi
}

# Parametres
# $1 le nom du fichier, exemple pour dirs_authorized-backoffice.txt on passe dirs_authorized
# $2 le nom du projet
copieFichierConfig() {
    if [ -e $modeleDirGitConfig/$1-$2.txt ]; then
        destination="$destinationDirGitDroits/$2/$1.txt"
        sudo cp $modeleDirGitConfig/$1-$2.txt $destination
        sudo chown git:git -R $destination
        sudo chmod 666 -R $destination

        echo -e "  [\033[32mOk\033[0m] $2 : Le fichier '$1' à été copié avec les bons droits"
    fi
}

generationLiensSymbolique(){
    cd $userWorkspace
    for lien in ${lienSymbolique[@]}; do
        cible="$dirHttpd/$lien"
        sudo ln -s $cible $lien
    done
    echo -e "  [\033[32mOk\033[0m] Création des liens symbolique"
}

adduser(){
    dateGeneration=$(date)

    getent passwd $userName  > /dev/null

    if [ $? -eq 0 ]; then
        echo -e "  [\033[31mKo\033[0m] l'utilisateur $userName existe déjà."
    else
        sudo -i
        useradd $userName -g $groupName -p $passwordDefault -c "Utilisateur créé via createWorkspace le $dateGeneration"
        echo -e "  [\033[32mOk\033[0m] Utilisateur $userName:$groupName créé avec le mdp par defaut '$passwordDefault'"
    fi

    exit 1
}

# ------------------------------------------------
# Main
# ------------------------------------------------
userWorkspace="$dirHttpd/$userDir"

if [ $(sudo whoami) != "root" ]; then
    echo "Votre utilisateur '$(whoami)' n'a pas les droits sudo."
    exit 1
fi

# adduser

cd $dirHttpd
echo -e "Création de l'environnement de travail pour l'utilisateur '\033[32m$userName\033[0m' :"
mkdir -p $userWorkspace
cd $userWorkspace
echo -e "  [\033[32mOk\033[0m] Création du workspace '$userWorkspace'"

for project in ${projects[@]}; do
    echo -e "  [\033[32mOk\033[0m] Clonage du project : \033[32m$project\033[0m"
    git clone $project
    last=$(ls -rt|tail -n1)
    cd $last
    git checkout develop
    cd ..

    # s'il existe on copie le fichier de vérification des droits
    if [ -f $modeleDirGitConfig/pre-commit-$project ]; then
        sudo cp $modeleDirGitConfig/pre-commit-$project $userWorkspace/$project/.git/hooks/pre-commit
        #suppression des retours à la ligne windows qui pourraient empecher l'execution et la divulgation du fichier
        sudo sed -i 's/\r//' $userWorkspace/$project/.git/hooks/pre-commit
        sudo chown -R $userName:$groupName $userWorkspace/$project/.git/hooks
        sudo chmod -R 555 $userWorkspace/$project/.git/hooks
        echo -e "  [\033[32mOk\033[0m] Application des bons droits"
    fi

    # s'ils existent on copie le fichier de configuration des droits
    for file in ${filesConfig[@]}; do
        if [ -f "$modeleDirGitConfig/$file-$project.txt" ]; then
            genereDirDroitProjet $project
            copieFichierConfig $file $project
        fi
    done
done

generationLiensSymbolique
sudo chown -R $userName:$groupName $userWorkspace

userGitConfig="/home/$userName/.gitconfig"
if sudo test ! -f "$userGitConfig"; then
    sudo cp $modeleGitConfig $userGitConfig
    #suppression des retours à la ligne windows qui pourraient empecher l'execution et la divulgation du fichier
    sudo sed -i 's/\r//' $userGitConfig
    sudo chown $userName:$groupName $userGitConfig
    sudo vim $userGitConfig
    echo -e "  [\033[32mOk\033[0m] Copie du fichier de configuration git"
fi


echo " .__(.)< ($1)"
echo "  \___)"

exit 0
