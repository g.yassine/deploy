 #!/bin/env bash

set -o pipefail
set -o errtrace
set -o nounset
set -o errexit
# to trace what gets executed.
# set -o xtrace

DIR_PROJECT="/data/sources"
# indiquer le nom des dossiers des projets à mettre à jour dans DIR_PROJECT
SITES=(
  "af" # OK: master
  "afim" # OK: master
  # "afin" # STAND BY, :warning: Branche dépréciée, il faut récuperer le code en prod pour l'apporter sur master
  # "aia" # STAND BY, :warning: Branche dépréciée, il faut récuperer le code en prod pour l'apporter sur master
  "ariane" # OK v1.6.6
  "backoffice" # OK: develop TODO faire un MR pour master
  "bo" # OK: master
  "core" # OK: master
  "Dockerfile" # OK: feature/clean-config-rancher-docker
  # "nc2" # STAND BY, master + cp d3a43a8c047ecbc5b0ed5f3f801ac87f87657237 ne doit pas être sur master à cause des serveurs prod non docker et non proxy
  "php_commun" # OK: master
  "portail" # STAND BY, :warning: Branche dépréciée, il faut récuperer le code en prod pour l'apporter sur master
  # "scpi" # STAND BY, master + cp bf67a6bf239239b080c193d97f981fc3f1c0fcec ne doit pas être sur master à cause des serveurs prod non docker et non proxy
  "studio" # OK: master
  "tools" # OK: develop TODO faire un MR pour master
)

# ------------------------------------------------
# Main
# ------------------------------------------------

if [ "${1:-}" = "--help" ] || [ "${1:-}" = "-h" ] || [ "$#" -gt 0 ]; then
    if [ "$#" -gt 0 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
    updatePra.sh - (2017-11-07)
    ==============================
    Met à jour les sites listée sur le pra
EOF
    exit 0
fi

echo ">>> Début de l'import [$(date)]"

for SITE in ${SITES[@]}; do
  echo "=============="
  echo "site : ${SITE}"
  cd "${DIR_PROJECT}/${SITE}"
  git status -sb
  git stash
  git fetch -p
  git co master
  git rebase
done

echo ">>> Fin [$(date)]"
exit 0
