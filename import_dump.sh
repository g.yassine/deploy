#!/usr/bin/env bash

set -o pipefail
set -o errtrace
set -o nounset
set -o errexit
# to trace what gets executed.
# set -o xtrace

PATH_DUMPS      = /data/oracle/dumps/
PATH_DPDUMPS    = /data/oracle/instance/dpdump/
PATH_DUMPS_PROD = /media/sicav-nas1/sauvegarde/db_dump/
DOCKER_ORACLE   = "oracle_db"
SERVEUR_BD      = "sicav-web0"
SERVEUR_BD_USER = "deploy"
ORACLE_USER     = dp
ORACLE_PASSWD   = zzb22x

# si le parametre 1 est --help alors affiche l'aide
if [ "${1:-}" = "--help" ] || [ "${1:-}" = "-h" ] || [ "$#" -lt 1 ]; then
    if [ "$#" -lt 1 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
    import_dump.sh
    ===============
    Dynamisation de l'import du dernier dump de prod + ajout du script de suppression des tables avant import.
    le 20/09/17 : l'upload du dump tar.gz prends ~23 min (24Go), son extraction ~15 min de plus.

    le second paramétre s'il est égal à :
    - firstExec : ne cherche pas à supprimer les structures et BDD qui n'existe pas lors de la première execution
    - noUpload : alors il n'y a pas de download du dernier dump depuis $SERVEUR_BD
    - retry : alors on utilise le dump déjà présent dans \$PATH_DUMPS ($PATH_DUMPS)

    Exemple d'utilisation :
    - execution
      $ ./import_dump.sh ORA11G [noUpload]
    - execution en arriere plan du traitement comples en mode one shoot
      $ sh /data/sources/deploy/import_dump.sh ORADEV >> /var/log/oracle-import/import_dump.log 2>&1 &

    Exemple de cron :
    - tous les jours à 04:00 :
      0 4 * * * sh /data/sources/deploy/import_dump.sh ORADEV >> /var/log/oracle-import/import_dump.log 2>&1
    - tous les dimanche à 04:00 :
      0 4 * * 7 sh /data/sources/deploy/import_dump.sh ORADEV >> /var/log/oracle-import/import_dump.log 2>&1
EOF
    exit 0
fi

echo ">>> Début de l'import [$(date)]"

# Nettoyage des anciens dump*
if [ -f last_dump_version.txt ] && [ "${2:-}" != "retry" ]; then
    echo "-- Nettoyage des anciens dump dans ${PATH_DPDUMPS}*dump_FULL.dmp* [$(date)]"
    rm -rf ${PATH_DPDUMPS}*dump_FULL.dmp*
    rm last_dump_version.txt
fi

# Nettoyage des anciens dump upload
if [ `ls ${PATH_DUMPS} | wc -l` -gt 2 ]; then
    echo "-- Nettoyage des anciens dump dans ${PATH_DUMPS}*dump_FULL.dmp* [$(date)]"
    rm -rf ${PATH_DUMPS}*dump_FULL.dmp*
fi

# Récupération du dernier dump de prod
if [ "${2:-}" != "retry" ]; then
    ssh -T ${SERVEUR_BD_USER}@${SERVEUR_BD} ls ${PATH_DUMPS_PROD} | tail -1 > last_dump_version.txt
fi

LAST_DUMP_VERSION=$(<last_dump_version.txt)
EXTRACT_FILENAME=$(echo ${LAST_DUMP_VERSION} | sed -e 's/.gz//g')

if [ "${2:-}" = "" ] || [ "${2:-}" = "firstExec" ]; then
    if [ ! -d "${PATH_DUMPS}" ]; then
        mkdir -p ${PATH_DUMPS}
    fi
    echo "-- Début de l'upload du dump ~23 min (24Go) ${PATH_DUMPS}${LAST_DUMP_VERSION} [$(date)]"
    rsync ${SERVEUR_BD_USER}@${SERVEUR_BD}:${PATH_DUMPS_PROD}${LAST_DUMP_VERSION} ${PATH_DUMPS}
    echo "-- fin de l'upload du dump ${LAST_DUMP_VERSION} [$(date)]"
fi

# Extraction

# Params oracle instance (ex ; ORA11G_PREPROD, ORA11G_CI )
ORACLE_INSTANCE=${1}

FILENAME="${EXTRACT_FILENAME}.gz"

echo "-- Début du traitement [$(date)]"

if [ -e "${PATH_DUMPS}${FILENAME}" ]; then
    CMD_EXTRACT="gunzip ${PATH_DUMPS}${FILENAME}"
    echo "-- Extraction du gunzip : ${CMD_EXTRACT} (~15min) [$(date)]"
    `${CMD_EXTRACT}`

    echo "-- Déplacement de ${PATH_DUMPS}${EXTRACT_FILENAME} vers ${PATH_DPDUMPS}${EXTRACT_FILENAME} [$(date)]"
    mv ${PATH_DUMPS}${EXTRACT_FILENAME} ${PATH_DPDUMPS}${EXTRACT_FILENAME}
fi

echo "-- Generation du parfile : ${PATH_DPDUMPS}${EXTRACT_FILENAME}.parfile [$(date)]"
cat << EOT > ${PATH_DPDUMPS}${EXTRACT_FILENAME}.parfile
dumpfile=${EXTRACT_FILENAME}
directory=DATA_PUMP_DIR
logfile=${EXTRACT_FILENAME}.log
EXCLUDE=SCHEMA:"IN('ANONYMOUS','APEX_PUBLIC_USER','APEX_030200','APPQOSSYS','COLT','CTXSYS','DBSNMP','DIP','DP','EXFSYS','EXPLOIT','FLOWS_FILES','MDDATA','MDSYS','MGMT_VIEW','OLAPSYS','ORACLE_OCM','ORDDATA','ORDPLUGINS','ORDSYS','OUTLN','OWBSYS','OWBSYS_AUDIT','SCOTT','SI_INFORMTN_SCHEMA','SPATIAL_CSW_ADMIN_USR','SPATIAL_WFS_ADMIN_USR','SYS','SYSMAN','SYSTEM','TRACESVR','TSMSYS','WMSYS','XDB','XS\$NULL')"
table_exists_action=replace
remap_datafile=/oracle/oradata/SICAV/sicav.dbf:/data/oracle/app/oradata/ORA11G/sicav.dbf,/oracle/oradata/SICAV/sol.dbf:/data/oracle/app/oradata/ORA11G/sol.dbf,/oracle/oradata/SICAV/ariane.dbf:/data/oracle/app/oradata/ORA11G/ariane.dbf,/oracle/oradata/SICAV/afc.dbf:/data/oracle/app/oradata/ORA11G/afc.dbf,/oracle/oradata/SICAV/af.dbf:/data/oracle/app/oradata/ORA11G/af.dbf,/oracle/oradata/SICAV/afim.dbf:/data/oracle/app/oradata/ORA11G/afim.dbf,/oracle/oradata/SICAV/afin.dbf:/data/oracle/app/oradata/ORA11G/afin.dbf,/oracle/oradata/SICAV/thes.dbf:/data/oracle/app/oradata/ORA11G/thes.dbf
FULL=Y
EOT

if [ "${2:-}" != "firstExec" ]; then
    echo "-- Suppression des séquences [$(date)]"
    CMD_DROP="docker exec -d ${DOCKER_ORACLE} sqlplus ${ORACLE_USER}/${ORACLE_PASSWD}@${ORACLE_INSTANCE} @/data/oracle/dpdump/drop_sequence.sql"
    echo "DEBUG: ${CMD_DROP}"
    `${CMD_DROP}`

    echo "-- Suppression des tables SICAV, SOL, ARIANE [$(date)]"
    CMD_DROP="docker exec -d ${DOCKER_ORACLE} sqlplus ${ORACLE_USER}/${ORACLE_PASSWD}@${ORACLE_INSTANCE} @/data/oracle/dpdump/drop_tables.sql"
    echo "DEBUG: ${CMD_DROP}"
    `${CMD_DROP}`
fi

echo "-- Début de l'import [$(date)]"
CMD_IMPORT="docker exec -d ${DOCKER_ORACLE} impdp ${ORACLE_USER}/${ORACLE_PASSWD}@${ORACLE_INSTANCE} parfile=/data/oracle/dpdump/${EXTRACT_FILENAME}.parfile exclude=statistics"
echo "DEBUG: ${CMD_IMPORT}"
`${CMD_IMPORT}`

# echo "-- Début du test de BDD [$(date)]"
# CMD_TEST="docker exec -i oracle_db sqlplus -s dp/zzb22x@ORADEV <<EOF
# SELECT TABLESPACE_NAME, count(*) FROM DBA_TABLES where TABLESPACE_NAME in ('SICAV','SOL') group by TABLESPACE_NAME order by TABLESPACE_NAME;
# quit
# EOF"
# echo "DEBUG: ${CMD_TEST}"
# `${CMD_TEST}`

echo ">>> Fin [$(date)]"

exit 0
