<?php
// Chemin où seront stockés les virtuals hosts
$pathGeneration = "/home/httpd/xh/vhosts";
$ipServeur      = "192.168.2.50";
$pathUserWorkspaces = "/home/httpd";

$users = [
    // Développeurs
    "ss-",
    "fj-",
    "gm-",
    "xh-",
    "sb-",
    "dd-",
    "zea-",
    "en-",
    "ac-",
    "nt-",
    "jo-",
    "od-",
    "wm-",
    "rav-",
    "al-",
    "zeh-",
	"kr-",
    // Equipe marketing
    "vr-",
    "yg-",
    "ae-",
    "ag-",
    "jl-",
    "sbm-",
    "hf-",
    "ec-",
    "fh-",
    "jm-",
    // Configuration par défaut, pour les dev-www, dev-back, ...
    ""
]; 

$caractereSupprimer="-";

$sites = [
    [
        'url'=>"af",
        'path'=> "avenir",
        'alias'=> ["newaf.sicavonline.fr"],
        'ports'=> [80],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"afim",
        'path'=> "afim",
        'alias'=> ["newafim.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"afin",
        'path'=> "afin",
        'alias'=> ["newafin.sicavonline.fr"],
        'ports'=> [80],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"agp",
        'path'=> "agp",
        'alias'=> ["newagp.sicavonline.fr"],
        'ports'=> [80],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"aia",
        'path'=> "advenis-ia",
        'alias'=> ["newaia.sicavonline.fr"],
        'ports'=> [80],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"clients",
        'path'=> "espace-clients",
        'alias'=> [],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice",  // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"]
        ]
    ],
    [
        'url'=>"sous-sol",
        'path'=> "souscription",
        'alias'=> ["souscription-dev.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice",  // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"]
        ]
    ],
    [
        'url'=>"sous-ageas",
        'path'=> "souscription",
        'alias'=> ["souscription-dev.ageas-patrimoine.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice",  // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"]
        ]
    ],
    [
        'url'=>"sous-advenis",
        'path'=> "souscription",
        'alias'=> ["souscription.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice",  // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"]
        ]
    ],
    [
        'url'=>"back",
        'path'=> "backoffice",
        'alias'=> ["newback.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice",  // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"]
        ]
    ],
    [
        'url'=>"bo",
        'path'=> "bo",
        'alias'=> ["newbo.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice", // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"]
        ]
    ],
    [
        'url'=>"nc2",
        'path'=> "nc2",
        'alias'=> ["newnc2.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"studio",
        'path'=> "studio",
        'alias'=> ["newstudio.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"scpi",
        'path'=> "scpi",
        'alias'=> ["newscpi.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"part",
        'path'=> "part",
        'alias'=> ["newpart.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"www",
        'path'=> "portail",
        'alias'=> ["newsol.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice", // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"],
        ]
    ],
    [
        'url'=>"mobile",
        'path'=> "mobile",
        'alias'=> ["newmobile.sicavonline.fr"],
        'ports'=> [80],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"ariane",
        'path'=> "ariane/web",
        'alias'=> ["newariane.sicavonline.fr"],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice", // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"],
            ]
    ],
    [
        'url'=>"trash",
        'path'=> "trash",
        'alias'=> [],
        'ports'=> [80],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ],
    [
        'url'=>"wishizz",
        'path'=> "wishizz",
        'alias'=> [],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice",  // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"]
        ]
    ],
    [
        'url'=>"motionway",
        'path'=> "motionway",
        'alias'=> [],
        'ports'=> [80, 443],
        'options' => [
            "SetEnv" => [PATH_CORE => "/home/httpd/#user#/core",
						PATH_BACKOFFICE => "/home/httpd/#user#/backoffice",  // #user# est le flag pour introduire le nom de l'utilistateur
						APPLICATION_ENV => "dev"]
        ]
    ],
    [
        'url'=>"exemple",
        'path'=> "exemple",
        'alias'=> [],
        'ports'=> [80],
        'options' => [
            "SetEnv" => [APPLICATION_ENV => "dev"]
            ]
    ]
];


/*
* %1$s = user.
* %2$s = url
* %3$s = ipServeur
* %4$s = user
* %5$s = path
* %6$s = ServerAlias
*/
$template80 = '

#
# %2$s
#
<VirtualHost %3$s:80>
    ServerName %1$sdev-%2$s
    %6$s
    DocumentRoot '.$pathUserWorkspaces.'/%5$s

    # Logs
    ErrorLog /var/log/httpd/%4$s-%2$s-error_log
    CustomLog /var/log/httpd/%4$s-%2$s-access_log common
</VirtualHost>';

/*
* %1$s = user.
* %2$s = url
* %3$s = ipServeur
* %4$s = user
* %5$s = path
* %6$s = ServerAlias
*/

$template443_autre = '

#
# %2$s SSL
#
<VirtualHost %3$s:443>
    ServerName %1$sdev-%2$s
    %6$s
    DocumentRoot '.$pathUserWorkspaces.'/%5$s

    # Logs
    ErrorLog /var/log/httpd/%4$s-%2$s-error_log
    CustomLog /var/log/httpd/%4$s-%2$s-access_log common

    #SSL
    SSLEngine on
    #SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL
    #test sb suite vulnerabilite FREAK
    #SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:-EXP:-eNULL
    #test sb suite audit securite
    SSLCipherSuite ALL:!aNULL:!eNULL:!EXP:!LOW:!MD5:!RC4:!DES:!3DES:+HIGH:+MEDIUM
    SSLHonorCipherOrder on
    SSLProtocol all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
    #Fin test
    SSLCertificateFile /usr/local/httpd/conf/ssl.crt/server.crt
    SSLCertificateKeyFile /usr/local/httpd/conf/ssl.key/server.key
    SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown
</VirtualHost>';
$template443_bo = '

#
# %2$s SSL
#
<VirtualHost %3$s:443>
    ServerName %1$sdev-%2$s
    %6$s
    DocumentRoot '.$pathUserWorkspaces.'/%5$s

    # Logs
    ErrorLog /var/log/httpd/%4$s-%2$s-error_log
    CustomLog /var/log/httpd/%4$s-%2$s-access_log common

    #SSL
    SSLEngine on
    #SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:+EXP:+eNULL
    #test sb suite vulnerabilite FREAK
    #SSLCipherSuite ALL:!ADH:!EXPORT56:RC4+RSA:+HIGH:+MEDIUM:+LOW:+SSLv2:-EXP:-eNULL
    #test sb suite audit securite
    SSLCipherSuite ALL:!aNULL:!eNULL:!EXP:!LOW:!MD5:!RC4:!DES:!3DES:+HIGH:+MEDIUM
    SSLHonorCipherOrder on
    SSLProtocol all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
    #Fin test
    SSLCertificateFile /usr/local/httpd/conf/extra/letsencrypt/bo.sicavonline.fr/bo.sicavonline.fr.cer
    SSLCertificateKeyFile /usr/local/httpd/conf/extra/letsencrypt/bo.sicavonline.fr/bo.sicavonline.fr.key
    SSLCertificateChainFile /usr/local/httpd/conf/extra/letsencrypt/bo.sicavonline.fr/fullchain.cer
    SetEnvIf User-Agent ".*MSIE.*" nokeepalive ssl-unclean-shutdown
</VirtualHost>';


/*
* %1$s = ipServeur
*/
$templateBase = '

### Section 3: Virtual Hosts
#
# VirtualHost: If you want to maintain multiple domains/hostnames on your
# machine you can setup VirtualHost containers for them. Most configurations
# use only name-based virtual hosts so the server doesn\'t need to worry about
# IP addresses. This is indicated by the asterisks in the directives below.
#
# Please see the documentation at
# <URL:http://httpd.apache.org/docs/2.2/vhosts/>
# for further details before you try to setup virtual hosts.
#
# You may use the command line option \'-S\' to verify your virtual host
# configuration.

#
# Use name-based virtual hosting.
#
#NameVirtualHost *:80
NameVirtualHost %1$s:80
NameVirtualHost %1$s:443
#
# NOTE: NameVirtualHost cannot be used without a port specifier
# (e.g. :80) if mod_ssl is being used, due to the nature of the
# SSL protocol.
#

#
# VirtualHost example:
# Almost any Apache directive may go into a VirtualHost container.
# The first VirtualHost section is used for requests without a known
# server name.
#
#<VirtualHost *:80>
#    ServerAdmin webmaster@dummy-host.example.com
#    DocumentRoot /www/docs/dummy-host.example.com
#    ServerName dummy-host.example.com
#    ErrorLog logs/dummy-host.example.com-error_log
#    CustomLog logs/dummy-host.example.com-access_log common
#</VirtualHost>

<VirtualHost %1$s:80>
    ServerName dummy
    #ServerAlias srvdev
    DocumentRoot /home/httpd/html

    ErrorLog /var/log/httpd/dummy-error_log
    CustomLog /var/log/httpd/dummy-access_log common
</VirtualHost>

#
# Administration de COLDFUSION
#
<VirtualHost %1$s:80>
    ServerName srvdev
    # Alias /CFIDE/administrator /home/httpd/CFIDE/administrator
    #Alias /CFIDE /home/httpd/CFIDE/
    #Alias /cfide /home/httpd/CFIDE/
    #Alias /CFIDE /usr/local/coldfusion/wwwroot/CFIDE
    DocumentRoot /home/httpd/html
    #DocumentRoot /usr/local/coldfusion/wwwroot/CFIDE
    #DocumentRoot /usr/local/coldfusion/wwwroot/cfide
    #DocumentRoot /home/httpd/cfide/administrator
    #DocumentRoot /home/httpd/wwwroot/cfide
    #DocumentRoot /home/httpd/backup
</VirtualHost>';

/*
* MAIN
*/
if(!file_exists($pathGeneration)){
    mkdir($pathGeneration);
}

foreach ($users as $user) {
    $retour = "# fichier généré par deploy.git/".__FILE__;
    if ($user == "") {
        $retour .= sprintf($templateBase, $ipServeur);
    }
    $userClean = str_replace($caractereSupprimer, "", $user);
    foreach ($sites as $infoSite) {
        $url     = $infoSite['url'];
        $path    = $infoSite['path'];
        $ports   = $infoSite['ports'];
        $alias   = $infoSite['alias'];
        $options = $infoSite['options'];

        if (file_exists("$pathUserWorkspaces/$userClean/$path")) {
            foreach ($ports as $port) {
                switch ($port) {
                    case 443:
						$template443 = ($path == "bo"?$template443_bo:$template443_autre);
                        $retour .= sprintf($template443, 
                                            $user, 
                                            $url, 
                                            $ipServeur, 
                                            $userClean,
                                            ($userClean == ""?$path:$userClean."/".$path), 
                                            ($user == "" && !empty($alias))?"ServerAlias ".implode(" ", $alias):""
                                        );
                        break;              
                    default:
                        $retour .= sprintf($template80, 
                                            $user, 
                                            $url, 
                                            $ipServeur, 
                                            $userClean,
                                            ($userClean == ""?$path:$userClean."/".$path), 
                                            ($user == "" && !empty($alias))?"ServerAlias ".implode(" ", $alias):""
                                        );
                        break;
                }

                if (!empty($options)) {
                    foreach ($options as $option=>$value) {
                        if($option == 'SetEnv'){
                            $tmp = ($userClean != "")?$userClean."/":"";
                            $setEnv = "";
                            foreach ($value as $key => $value) {
                                $setEnv .= "    SetEnv $key \"".str_replace("#user#/", $tmp, $value)."\"\r\n";
                            }
                            $derniereBalise = substr($retour, strrpos($retour, "<"));
                            $avantDerniereBalise = substr($retour, 0, strrpos($retour, "<"));
                            $retour = $avantDerniereBalise.$setEnv.$derniereBalise;
                        }
                    }
                }
            }
        }


    }
    $fp = fopen($pathGeneration.'/httpd-vhosts'.(($user == "")?"":$caractereSupprimer. $userClean).'.conf', 'w');
    fwrite($fp, trim($retour));
    fclose($fp);
}

echo sprintf('Fin de generation des %1$d fichiers', count($users));
