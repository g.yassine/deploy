==================================================
Intégration continu et Déploiement continu (CI/CD)
==================================================

Afin de comprendre le principe mis en place chez sicav nous allons explorer la mise en oeuvre du projet `deploy <http://gitlab-sicav/it-sol/deploy>`_. `Jenkins <http://jenkins-sicav/>`_ est utilisé sur ce projet pour le tester automatiquement  (CI : Continuous integration) et lors d'un push pour déployer automatiquement la derniere version de la branche develop sur srvdev si les tests sont OK (CD : continuous deployment).

Déclenchement des pipelines
===========================

Gitlab est paramétré pour envoyer un webhook au job `deploy-pipeline <http://jenkins-sicav/job/deploy-pipeline>`_ de jenkins. 

.. image:: ../../../resources/gitlabParametreWebhookJenkinsPipeline.jpg
    :scale: 100%
    :align: center

CI et CD
========

Ce job déclenche lui-même 2 autres jobs qui s'enchainent si tout est OK !

* `deploy-develop <http://jenkins-sicav/job/deploy-develop>`_ est le job CI qui test que deployDev.sh fonctionne 
* `deploy-deploiement-srvdev <http://jenkins-sicav/job/deploy-deploiement-srvdev>`_ est le job CD qui déploie la dernière version du projet deploy sur srvdev 
 
.. image:: ../../../resources/pipelineJobDeploy.jpg
    :scale: 100%
    :align: center

A savoir que les job de CI sont effectués grâce à des tests éxécutés sur un system "fictif" créé à base d'un contenaire Docker. Dans ce container est joué un test avec l'outil `goss <https://github.com/aelsabbahy/goss>`_ , pour tester les containers docker il faut utiliser le wrapper dgoss de ce même projet.
Les instructions utilisables pour tester un environnement ici plus spécifiquement : les déploiements, sont documentées dans la `documentation officielle de goss<https://github.com/aelsabbahy/goss/blob/master/docs/manual.md>`_

Tests spécifiques des déploiements
==================================

* Le contenaire est créé grâce à ce `Dockerfile <http://gitlab-sicav/it-sol/deploy/blob/develop/Dockerfile>`_
* Les tests sont enregistrés dans ce fichier `goss.yaml <http://gitlab-sicav/it-sol/deploy/blob/develop/tests/assets/goss.yaml>`_. Le système goss utilisé pour testé est installable `ainsi <https://github.com/aelsabbahy/goss/tree/master/extras/dgoss#installation>`_, si l'on souhaite ajouter des tests il faut utiliser cette commande :

.. code-block:: bash 

    vagrant@DEV-VM ~/projects/deploy/tests/assets (git)-[develop] % dgoss edit --rm -it registry.intranet.ageas.fr:5000/deploy-test:deploy-test-sicav bash
    INFO: Starting docker container
    INFO: Container ID: 3c10c34d
    INFO: Run goss add/autoadd to add resources
    ...
    sh-4.2$ exit
    INFO: Deleting container
    130 vagrant@DEV-VM ~/projects/deploy/tests/assets (git)-[develop] % 

Cette commande permet de tester la derniere image stable générée par le job 'deploy-develop'

.. code-block:: bash

    vagrant@DEV-VM ~/projects/deploy (git)-[develop] % docker run --rm registry.intranet.ageas.fr:5000/deploy-test:deploy-test-sicav goss v    :(
    ............................................................................................
    
    Total Duration: 0.002s
    Count: 92, Failed: 0, Skipped: 

Badges de jobs jenkins
======================

Enfin pour obtenir rapidement un apercu de l'état des CI / CD depuis les repository git.

.. image:: ../../../resources/gitlabBuildStatusMarkdown.jpg
    :scale: 100%
    :align: center

Il a été installé un plugin jenkins (embeddable-build-status) qui permet d'ajouter dans les readme.md (markdown) des différents projets un badge indiquant si tout s'est bien passé !

.. image:: ../../../resources/jenkinsBuildStatusMarkdown.jpg
    :scale: 100%
    :align: center
