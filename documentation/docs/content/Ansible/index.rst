=======
Ansible
=======

Introduction
------------

Ansible est un logiciel de déploiement/configuration à distance, créé par M. De Hann ( Redhat : Cobbler et Func)
et est écrit en python. Il utilise seulement SSH et ne nécessite pas de serveur : une simple station de travail
peut suffire. Il est bon de noter qu'il fonctionne en mode push (de Ansible vers le serveur cible).

La machine hébergeant Ansible ne nécessite que python 2.4+, et Ansible est extensible en n'importe quel langage.
Cette solution est plutôt dédiée à un usage professionnel.

.. toctree::
    :maxdepth: 3

    installation
    utilisation