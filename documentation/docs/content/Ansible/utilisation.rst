=================
Utiliser Ansible
=================

Tester la communication avec les serveurs
-----------------------------------------

.. code-block:: bash

    ansible -i hosts -m ping all

All ici signifie tous les hôtes :

Si l'on ne spécifie pas '-i hosts' pour désigner l'inventory (le fichier 'hosts' qui va être utilisé) il se peut que l'on tombe sur cette erreur, car par défaut ansible cherche son fichier dans /etc/ansible/hosts

.. code-block:: bash

    [WARNING]: Host file not found: /etc/ansible/hosts

    [WARNING]: provided hosts list is empty, only localhost is available

Le résultat doit-être celui-ci :

.. code-block:: bash

    sicav-dev1 | success >> {
        "changed": false,
        "ping": "pong"
    }

    sicav-preprod1 | success >> {
        "changed": false,
        "ping": "pong"
    }

    sicav-web4 | success >> {
        "changed": false,
        "ping": "pong"
    }

    srvdev | success >> {
        "changed": false,
        "ping": "pong"
    }

On mode on-liner
----------------

Pour tester le bon fonctionnement, on va installer **htop** sur *sicav-dev1* en utilisant le module yum:

.. code-block:: bash

    ansible -i hosts -m yum -a 'name=htop' sicav-dev1


La console affiche l'état des opérations

.. code-block:: bash

    sicav-dev1 | success >> {
        "changed": true,
        "msg": "warning: /var/cache/yum/x86_64/7/epel/packages/htop-1.0.3-3.el7.x86_64.rpm: Header V3 RSA/SHA256 Signature, key ID 352c64e5: NOKEY\nImporting GPG key 0x352C64E5:\n Userid     : \"Fedora EPEL (7) <epel@fedoraproject.org>\"\n Fingerprint: 91e9 7d7c 4a5e 96f1 7f3e 888f 6a2f aea2 352c 64e5\n Package    : epel-release-7-2.noarch (@extras)\n From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7\n",
        "rc": 0,
        "results": [
            "Loaded plugins: fastestmirror, langpacks\nLoading mirror speeds from cached hostfile\n * base: mirror.ate.info\n * epel: epel.mirrors.ovh.net\n * extras: mirror.ate.info\n * updates: mirror.skylink-datacenter.de\nResolving Dependencies\n--> Running transaction check\n---> Package htop.x86_64 0:1.0.3-3.el7 will be installed\n--> Finished Dependency Resolution\n\nDependencies Resolved\n\n================================================================================\n Package         Arch              Version                Repository       Size\n================================================================================\nInstalling:\n htop            x86_64            1.0.3-3.el7            epel             87 k\n\nTransaction Summary\n================================================================================\nInstall  1 Package\n\nTotal download size: 87 k\nInstalled size: 181 k\nDownloading packages:\nPublic key for htop-1.0.3-3.el7.x86_64.rpm is not installed\nRetrieving key from file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7\nRunning transaction check\nRunning transaction test\nTransaction test succeeded\nRunning transaction\n  Installing : htop-1.0.3-3.el7.x86_64                                      1/1 \n  Verifying  : htop-1.0.3-3.el7.x86_64                                      1/1 \n\nInstalled:\n  htop.x86_64 0:1.0.3-3.el7                                                     \n\nComplete!\n"
        ]
    }


Utiliser les playbooks
----------------------

Un playbook est une sorte de mégascript qui va automatiser des tâches de manière séquentielle.

Globalement, un playbook est composé de tâches comme ceci :

.. code-block:: bash

    - name: Texte qui décris votre tâche
      module: option=value

Plus d'infos sur les modules : http://docs.ansible.com/list_of_all_modules.html

Un exemple simple
^^^^^^^^^^^^^^^^^

Voici une illustration avec un playbook "test" pour installer htop :

.. code-block:: yaml

    # htop.yml
    ---
    - hosts: sicav-dev1
      tasks:
        - name: 1. install htop
          portage: name=htop state=present


Ici, je vise les hôtes du groupe **webservers**, et le playbook ne comporte qu'une seule tâche.

On lance le playbook via la commande

.. code-block:: bash

    ansible-playbook -i hosts htop.yml

.. code-block:: bash

    PLAY [webservers] ****************************************************************

    GATHERING FACTS ***************************************************************
    ok: [sicav-dev1]
    ok: [sicav-preprod1]
    ok: [sicav-web4]

    TASK: [1. install htop] *******************************************************
    changed: [sicav-dev1]
    changed: [sicav-preprod1]
    changed: [sicav-web4]

    PLAY RECAP ********************************************************************
    sicav-dev1                 : ok=3    changed=1    unreachable=0    failed=0
    sicav-preprod1             : ok=3    changed=1    unreachable=0    failed=0
    sicav-web4                 : ok=3    changed=1    unreachable=0    failed=0

Une autre expérience intéressante consiste à relancer l’exécution du playbook

.. code-block:: bash

    PLAY [webservers] ****************************************************************

    GATHERING FACTS ***************************************************************
    ok: [sicav-dev1]
    ok: [sicav-preprod1]
    ok: [sicav-web4]

    TASK: [1. install htop] *******************************************************
    ok: [sicav-dev1]
    ok: [sicav-preprod1]
    ok: [sicav-web4]

    PLAY RECAP ********************************************************************
    sicav-dev1                 : ok=3    changed=0    unreachable=0    failed=0
    sicav-preprod1             : ok=3    changed=0    unreachable=0    failed=0
    sicav-web4                 : ok=3    changed=0    unreachable=0    failed=0



Tout devrait aller beaucoup plus vite, et à la place de "changed" après chaque instruction, on lit "ok".
Ce qui veut dire qu’un playbook est plus intelligent qu’un bête script, et ne se contente pas d’exécuter des instructions.
Ansible va garantir quel tel service soit bien actif et qu’il utilise bien le dernier fichier de conf.
Ce qui en fait l’outil parfait pour tester vos systèmes automatiquement.