======================
Installation d'Ansible
======================

Il est nécessaire d'installer  `les dépôts EPEL <http://www.linuxtricks.fr/wiki/centos-ajouter-des-depots-supplementaires>`_.

.. code-block:: bash

    yum install ansible

Préparer le terrain
-------------------

Configurer le serveur Ansible
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sur la machine qui possède Ansible, configurer le fichier /etc/hosts avec le nom des hôtes :

.. code-block:: bash

    infra ~ # cat /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
    127.0.0.1 DEVELOPMENT-VM DEV-VM
    192.168.2.50 srvdev
    192.168.10.3 sicav-web0
    192.168.10.19 sicav-web4
    192.168.10.21 sicav-tools
    192.168.10.52 sicav-dev1
    192.168.10.53 sicav-recette1
    192.168.10.54 sicav-preprod1 registry.intranet.ageas.fr
    192.168.10.55 sicav-rh6
    192.168.10.56 sicav-rh7
    192.168.10.150 sicav-bd4
    192.168.11.3 sicav-bd3

Configurer le SSH
^^^^^^^^^^^^^^^^^

On génère ensuite une paire de clés SSH

.. code-block:: bash

    infra ~ # ssh-keygen
    Generating public/private rsa key pair.
    Enter file in which to save the key (/root/.ssh/id_rsa):
    Created directory '/root/.ssh'.
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /root/.ssh/id_rsa.
    Your public key has been saved in /root/.ssh/id_rsa.pub.
    The key fingerprint is:
    d8:de:8e:f9:a4:44:07:a7:ec:dc:ed:81:10:ef:1e:b8 root@rhelsrv1
    The key's randomart image is:
    +--[ RSA 2048]----+
    |                 |
    |                 |
    |        o .      |
    |       + *       |
    |      . S o      |
    |       = B o     |
    |        * B o    |
    |       . O o .   |
    |        E.+ .    |
    +-----------------+

Puis on copie notre clé sur chaque serveur :

.. code-block:: bash

    infra ~ # ssh-copy-id sicav-tools
    infra ~ # ssh-copy-id sicav-dev1
    infra ~ # ssh-copy-id sicav-recette1
    ssh-copy-id sicav-tools
    ssh-copy-id sicav-rh6 &&
    ssh-copy-id sicav-rh7
    ...

On peut vérifier le bon fonctionnement en se connectant à chacune des machines...

Configuration des Host Ansible
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. tip:: Ceux-ci sont normalement déjà configurer dans le playbook du projet dans le ficher ``playbook/hosts``

.. code-block:: bash

    [webservers]
    sicav-dev1		    ansible_host=sicav-dev1  ansible_connection=ssh	ansible_user=adminsol
    sicav-recette1		ansible_host=sicav-recette1  ansible_connection=ssh	ansible_user=adminsol
    sicav-web4		    ansible_host=sicav-web4  ansible_connection=ssh	ansible_user=adminsol

    [old_server]
    srvdev 		        ansible_host=srvdev   ansible_connection=ssh   ansible_user=info

Entre crochet, on trouve les groupes d'hôtes, avec les hôtes concernés en dessous.

Ici, j'ai donc 2 groupes, un **webservers** avec les hôtes *sicav-dev1, sicav-preprod1, sicav-web4*
et le groupe **old_server** avec *srvdev*

Pour lire les disques des serveurs :

.. code-block:: bash

    % ansible -i hosts pra,tools -m setup -a 'filter=ansible_devices'

Etre sûr de pouvoir jouer des commandes sudo sans mot de passe via le /etc/sudoers de chaque serveur

.. code-block:: bash

    % ansible -i hosts pra,tools -m shell -a 'sudo cat /etc/sudoers|grep NOPASSWD'
    sicav-tools | SUCCESS | rc=0 >>
    %wheel  ALL=(ALL)       NOPASSWD: ALL

    sicav-rh6 | SUCCESS | rc=0 >>
    adminsol ALL=(ALL)      NOPASSWD: ALL
    #%wheel ALL=(ALL)       NOPASSWD: ALL

    sicav-rh7 | SUCCESS | rc=0 >>
    %wheel ALL=(ALL)       NOPASSWD: ALL



.. code-block:: bash

    % ansible -i hosts sicav-tools -m shell -a 'df -h|grep /data'
    sicav-tools | SUCCESS | rc=0 >>
    /dev/sde        246G   61M  234G   1% /data


Avant le btrfs

.. code-block:: bash

    % ansible -i hosts pra,tools -m shell -a 'df -h|grep /data'                                                                                           :(
    sicav-tools | SUCCESS | rc=0 >>
    /dev/sde        246G   61M  234G   1% /data

    sicav-rh7 | SUCCESS | rc=0 >>
    /dev/sdd1       148G   61M  140G   1% /data

    sicav-rh6 | SUCCESS | rc=0 >>
    /dev/sdc1       148G   60M  140G   1% /data