======================================
Ajout d'un serveur
======================================

Scénario nominal
----------------

1. Pour Ajouter un server, vous devez éditer le fichier d'inventaire des machines ``playbook/hosts`` (voir la `documentation <https://docs.ansible.com/ansible/intro_inventory.html>`_)

2. Créer ensutie un fichier au nom du serveur dans le dossier ``playbook/hosts_vars``

.. epigraph::

    Par exemple pour le server *sicav-r7*  je créer un fichier ``playbook/hosts_vars/sicav-r7``

3. Ajouter dans le fichier les variables suivantes :

.. code-block:: bash

    SERVER:
      hostname: <PETIT-NOM-DU-SERVEUR>

    MAIL:
      relayhost: '<IP_OU_DNS_DU_SERVEUR_DE_MAIL>'

.. attention:: N'oubliez pas de personnaliser les variables

Scénario complémentaire
-----------------------

Si vous avez ajouter un nouveau groupe de serveur, pensez à créer le fichier de configuration du groupe dans ``playbook/group_vars``