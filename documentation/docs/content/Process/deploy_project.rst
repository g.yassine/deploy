======================================
Déploiement d'un projet full conteneur
======================================

Préparation des containeurs du projet
-------------------------------------

L'idée est de "packagé" votre application au sein de conteneur(s) qui seront immutables et déployable à l'infini sur des environnements dockerisés.

Le projet doit être industrialisé et pour se faire un fichier ``./Makefile`` existe surement.  L'exécution de la commande ``make`` va vous montrer les possibilitées de votre projet.

.. code-block:: bash

    vagrant@DEV-VM:~/projects/ws
    ▶ make
    \Found Makefile directory: /home/vagrant/projects/ws
    help                Affiche l'aide
    install             Installe le projet
    up                  Demarrage des containers
    down                Stop et détruits les containers
    restart             Reboot the stack
    logs                Affiche les logs de la stack
    version             Affiche la version de la stack
    tests               Lance les tests d'infra pour savoir si la stack de dev est fonctionnelle
    passwd              Demande la saisie du mot de passe oracle
    update-db           Fait les maj necessaire dans la base
    composer            Execute composer
    console             Execute la commande
    bash                Connection au ssh du container
    fix                 Fix le code avec phpcs-fixer
    analyse             Fix le code avec phpcs-fixer
    phpunit             Execute les tests unitaire
    phpspec             PHPSpec
    fixture             Charge les fixtures de developpement
    proxy-up            Démarre le proxy
    create-network      Crée le network projets_frontend, True à modifier
    build               Alias de build-all
    build-all           Construit les images php et app du projet en version de dev
    docker-push         Utilise l'image de dev:${DOCKER_TAG_DEV} pour la push avec un tag de recette ${DOCKER_TAG_GIT_BRANCH}
    docker-push-prod    Utilise l'image de dev:${DOCKER_TAG_DEV} pour la push avec un tag de prod ${DOCKER_TAG_PRD}
    docker-login        Permet de se connecter au registry docker

Par exemple sur ce projet la commande à executer en dev est ``make up`` ainsi les conteneurs demarrent correctement et permettent d'exploiter les sources de votre vm depuis l'url défini dans votre application mis à disposition par votre proxy sur le port ``80`` administrable ici ``vm:8080``. Je n'invente rien ou n'ai pas plus de connaissance que toi lecteur, simplement je lis (bravo à toi de faire ceci également ! Continue ;) ) !

.. code-block:: bash

    vagrant@DEV-VM:~/projects/ws  hotfix/502-restart-php
    ▶ make up
    \Found Makefile directory: /home/vagrant/projects/ws
    Error response from daemon: network with name projets_frontend already exists
    Creating network "ws_default" with the default driver
    Creating proxy ... done
    [Wed Mar 27 15:27:44 UTC 2019] Demarrage des containers
    Creating ws_php_1 ... done
    ws_php_1 is up-to-date
    Creating ws_app_1 ... done

    vagrant@DEV-VM:~/projects/ws  hotfix/502-restart-php
    ▶ make logs
    \Found Makefile directory: /home/vagrant/projects/ws
      Name                Command                       State                                        Ports
    ------------------------------------------------------------------------------------------------------------------------------------
    proxy      /entrypoint.sh --api --docker    Up                      0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp, 0.0.0.0:8080->8080/tcp
    ws_app_1   nginx -g daemon off;             Up                      443/tcp, 80/tcp
    ws_php_1   /usr/local/bin/docker-entr ...   Up (health: starting)   9000/tcp
    Attaching to ws_app_1, ws_php_1, proxy
    php_1    | -> Executing /usr/local/bin/service.d/entrypoint.d/10-create-user.sh
    php_1    | -> Executing /usr/local/bin/service.d/entrypoint.d/20-xdebug.sh
    php_1    | * Activate Xdebug
    php_1    | 2019/03/27 15:27:50 immortal scandir: /usr/local/etc/immortal
    php_1    | 2019/03/27 15:27:50 Starting: php-fpm

Création des images du projet
-----------------------------

Une fois l'application stable il est temps de créer les images à deployer !
``make help`` nous indique que la commande est ``make build-all`` :

.. code-block:: bash

    vagrant@DEV-VM:~/projects/ws  hotfix/502-restart-php
    ▶ make help
    \Found Makefile directory: /home/vagrant/projects/ws
    help                Affiche l'aide
    ...
    build               Alias de build-all
    build-all           Construit les images php et app du projet en version de dev

Donc c'est parti :

.. code-block:: bash

    vagrant@DEV-VM:~/projects/ws  hotfix/502-restart-php
    ▶ make build-all
    \Found Makefile directory: /home/vagrant/projects/ws
    N'oubliez pas de saisir le mot de passe oracle dans le fichier './passwd-oracle'
    Génération des images à partir du fichiers docker-compose.yml :
        image: registry.intranet.ageas.fr:5000/ws-php:alpha
        image: registry.intranet.ageas.fr:5000/ws-sol:alpha
    Utilisation du fichier .env.dist comme fichier d'environnement
    Building app
    Step 1/8 : FROM nginx:1.11-alpine
    ---> bedece1f06cc
    Step 2/8 : WORKDIR /srv/app
    ---> Using cache
    ---> 91d55493f08b
    Step 3/8 : COPY ./docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
    ---> Using cache
    ---> e61c1f4c59df
    Step 4/8 : COPY . /srv/app/
    ---> 13df14a5280e
    Removing intermediate container b7e53f4a6246
    Step 5/8 : ARG VERSION_BUILD_DATE=NULL
    ---> Running in 602f750c1c2f
    ---> e877b97f536a
    Removing intermediate container 602f750c1c2f
    Step 6/8 : ARG VERSION_CURRENT_BRANCH=NULL
    ---> Running in eb09fb7e9f14
    ---> 11d049a92ffb
    Removing intermediate container eb09fb7e9f14
    Step 7/8 : ARG VERSION_COMMIT_HASH=NULL
    ---> Running in 8df03a5020b1
    ---> c8aeb90cdcc7
    Removing intermediate container 8df03a5020b1
    Step 8/8 : ENV VERSION_BUILD_DATE $VERSION_BUILD_DATE VERSION_CURRENT_BRANCH $VERSION_CURRENT_BRANCH VERSION_COMMIT_HASH $VERSION_COMMIT_HASH
    ---> Running in 2909801a4ff8
    ---> 12ef5808cadc
    Removing intermediate container 2909801a4ff8

    Successfully built 12ef5808cadc
    Successfully tagged registry.intranet.ageas.fr:5000/ws-sol:alpha
    Building php
    Step 1/9 : FROM registry.gitlab.com/sicavonline/portail/php-oci:latest-stable                                                      -
    ---> 980116e75101                                                                                                                 p
    Step 2/9 : WORKDIR /srv/app
    ---> Using cache
    ---> 316681760f52
    Step 3/9 : COPY docker/php-scripts/10-create-user.sh /usr/local/bin/service.d/entrypoint.d/10-create-user.sh
    ---> 3b316d430468
    Removing intermediate container beb9a5c4b28c
    Step 4/9 : COPY docker/php-scripts/20-xdebug.sh /usr/local/bin/service.d/entrypoint.d/20-xdebug.sh
    ---> c5a83d5f7023
    Removing intermediate container 8ae2cb8d4f0b
    Step 5/9 : COPY . /srv/app
    ---> b1ed430854b9
    Removing intermediate container 54c152b906e5
    Step 6/9 : ARG VERSION_BUILD_DATE=NULL
    ---> Running in 26a3c80e1f6f
    ---> 07aa1ff7c63b
    Removing intermediate container 26a3c80e1f6f
    Step 7/9 : ARG VERSION_CURRENT_BRANCH=NULL
    ---> Running in b51f2770de4f
    ---> a94a9b2de9a9
    Removing intermediate container b51f2770de4f
    Step 8/9 : ARG VERSION_COMMIT_HASH=NULL
    ---> Running in dcdfd0be8af6
    ---> d91aa54bcd2e
    Removing intermediate container dcdfd0be8af6
    Step 9/9 : ENV VERSION_BUILD_DATE $VERSION_BUILD_DATE VERSION_CURRENT_BRANCH $VERSION_CURRENT_BRANCH VERSION_COMMIT_HASH $VERSION_COMMIT_HASH
    ---> Running in 84da18334514
    ---> 91030f114860
    Removing intermediate container 84da18334514
    Successfully built 91030f114860
    Successfully tagged registry.intranet.ageas.fr:5000/ws-php:alpha

  Cette commande va créer ici 2 images ``registry.intranet.ageas.fr:5000/ws-sol:alpha`` et ``registry.intranet.ageas.fr:5000/ws-php:alpha`` qui seront **aussi** exploitées en dev.

Tester les images
-----------------

Il faut être sûr que l'image soit viable, il peut y avoir des tests systems du genre de ``make tests`` (Lance les tests d'infra pour savoir si la stack de dev est fonctionnelle). Une fois des conteneurs créés et sûr il faut tester la version recetable, c'est à dire sans les sources de votre environnement de dev.

Pour se faire vous devez desactiver les points de montage (volumes docker) qui utilisent votre code pour cette application. Allez dans le fichier ``docker-compose.yml`` et commentez les lignes :

.. code-block:: bash

    vagrant@DEV-VM:~/projects/ws  hotfix/502-restart-php
    ▶ cat docker-compose.yml
    version: "3.5"
    volumes:
      db-data: {}
    ...
    services:
      php:
      ...
        # volumes:
        #   - ./:/srv/app/
      app:
      ...
        image: registry.intranet.ageas.fr:5000/ws-sol:alpha
        # volumes:
        #   - ./docker/nginx/conf.d:/etc/nginx/conf.d:ro
        #   - ./:/srv/app/:ro

Deployer le nouveau projet
--------------------------

.. warning:: Il est INDISPENSABLE de faire attention à l'environnement sur lequel on se place pour par exemple ne pas supprimer de la production quand on crois déployer en recette

Pour se faire rendez-vous dans le catalogue sicav (clean-config à ce jour) pour déployer votre projet

.. image:: ../../../resources/deploy-rancher/catalogue.JPG
    :align: center
    :scale: 50 %
    :alt: catalogue

Saissisez toutes les valeurs demandées. Il peut être intéressant de lire le docker-compose disponible en ouvrant l'accordéon PREVIEW pour s'assurer du résultat qui sera généré aprés votre saisie.

.. image:: ../../../resources/deploy-rancher/deploiement-de-la-stack.JPG
    :align: center
    :scale: 50 %
    :alt: deploiement-de-la-stack

La stack nouvellement déployée sera consultable dans le menu stack/User

.. image:: ../../../resources/deploy-rancher/stack-deployee.JPG
    :align: center
    :scale: 50 %
    :alt: stack-deployee

Si vous souhaitez mettre à jour une stack déjà existante il faut passer par l'option upgrade disponible dans le menu stack/User.

.. image:: ../../../resources/deploy-rancher/upgrade-stack.JPG
    :align: center
    :scale: 50 %
    :alt: upgrade-stack

Les options à connaitre pour upgrade sont :

1. elle permet de démarrer en état stable la nouvelle version avant de stopper la version actuelle
2. il est important de s'assurer de gérer tous les services de la stack
3. cette option permet de pull l'image de son repository docker avant de la déployer, ceci n'est pas à faire systematiquement pensez bien cette option avant de l'utiilser ou non.
4. désignez correctement l'image ET le tag cible
5. lors d'utilisation de proxy tel que traefik ou autre system avancés, pensez à faire le tour des options tel que les labels ``traefik.domain``, ``traefik.frontend.rule`` ou les HealthCheck, Secrets, ...

.. image:: ../../../resources/deploy-rancher/icon-rancher.png
    :align: left
    :height: 40px
    :width: 40px
    :alt: rancher
Refs
----

`Ancien rancher <http://rancher-sicav>`_

`Nouveau rancher à jour toujours en version 1.6.x <http://sicav-preprodbd:8080>`_
