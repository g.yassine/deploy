.. _ref-create-profiles:

====================
Création d'un profil
====================

.. note:: Cette procédure est uniquement valable pour ``sicav-dev1``

.. attention:: Le projet *Deploy* ne s'occupe que du provisionning "système" de la machine.
    Pour configurer les projets des utilisateur, il faut se référeer à la documentation du projet Dockerfile

Création du profil
^^^^^^^^^^^^^^^^^^

Pour créer un profil sur la machine il vous suffit de mettre à jour le fichier ``profiles.yml``
( voir :ref:`ref-syntax-profiles`) et de relancer le playbook.

.. code-block:: bash

    cd /path/of/project/deploy
    cd playbook
    vim profiles.yml
    ansible-playbook -i hosts --limit sicav-dev1 webservers.yml

Configuration de l'utilisateur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Pour configurer les projets des nouveaux utilisateurs, je vous invite à lire la documentation du projet Dockerfile
dans la rubrique Docker-Compose.