===========================
Playbook Sicavonline
===========================

Ce *Playbooks* sert à configurer et installer les différents serveurs de production, préproduction et intégration
utiliser par Sicavonline.

Structure des fichiers
----------------------

.. include:: include/structures.rst

Comment utiliser ce playbook
----------------------------

.. include:: include/command.rst

Roles
-----

.. toctree::
    :maxdepth: 1

    roles/clitools
    roles/common
    roles/docker
    roles/finalize
    roles/git
    roles/logrotate
    roles/mail
    roles/monit
    roles/motd
    roles/ntp
    roles/profiles
    roles/samba
    roles/ssh
    roles/user

