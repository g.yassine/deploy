Provisionner le serveur de developpement
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

    ansible-playbook -i hosts --limit sicav-dev1 webservers.yml

Provisionner le serveur de préproduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

    ansible-playbook -i hosts --limit sicav-preprod1 webservers.yml

Provisionner le serveur de production
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

    ansible-playbook -i hosts --limit sicav-web4 webservers.yml

Tester sans provisionner le serveur
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

    ansible-playbook -i hosts --limit sicav-preprod1 webservers.yml -C

.. attention:: Ne marche pas toujours jusqu'au bout. Car certaines tâche son dépendantes les unes des autres

Provisionner le serveur de pré-production et production
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

    ansible-playbook -i hosts --limit sicav-preprod1,sicav-web4 webservers.yml


# execute test
^^^^^^^^^^^^^^

.. code-block:: bash

    testinfra -v --connection=ansible --ansible-inventory ./hosts --hosts=vostok tests/*