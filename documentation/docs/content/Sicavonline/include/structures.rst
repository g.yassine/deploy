Structure du playbook
^^^^^^^^^^^^^^^^^^^^^

======================================= ====================================================================
Répertoire                              Description
======================================= ====================================================================
``host``                                Fichier d'inventaire des machines
``group_vars/webservers``               Ce fichier sert à définir des variables pour le group webservers
``host_vars/sicav-dev1``                Ce fichier sert à définir des variables pour la machine sicav-dev1
``host_vars/sicav-preprod1``            Ce fichier sert à définir des variables pour la machine sicav-preprod1
``host_vars/sicav-web4``                Ce fichier sert à définir des variables pour la machine sicav-web4
``webservers.yml``                          Le fichier principal de configuration du playbook
``profiles.yml``                        Le fichier de configuration des utilisateur de l'environnement
                                        d'intégration
``roles``                               Répertoire contenant les rôles exécuter par le playbook
======================================= ====================================================================

Structure d'un rôle
^^^^^^^^^^^^^^^^^^^

======================================= ====================================================================
Répertoire                              Description
======================================= ====================================================================
``tasks/main.yml``                      Fichier de tâches principal exécuter par le rôle
``handlers/main.yml``                   Fichier contenant les 'handlers' (écouteurs d'evènements)
``templates``                           Répertoire contenant les modèles utilisé dans les tâches
                                        pour générer des fichiers (fichier au format jinja => équivalent
                                        de twig en python)
``files``                               Répertoire contenant des fichiers à déposé sur les serveurs
======================================= ====================================================================

Pour en savoir plus sur les `playbooks <https://docs.ansible.com/ansible/playbooks.html>`_