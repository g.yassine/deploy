=============
Rôle Clitools
=============

Ce rôles permet d'installer **CliTools**.

CliTools est un utilitaire terminal pour un développement plus rapide.
CliTools est basé sur des composants Symfony (console).

Pour en savoir plus allez voir le `site de l'auteur <https://github.com/webdevops/clitools>`_

