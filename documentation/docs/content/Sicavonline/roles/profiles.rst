======================
Rôle profiles
======================

Ce rôle est dédié au serveur d'intégration ``sicav-dev1``.
Il permet de créer et configurer les utilisateurs du serveur.

.. _ref-syntax-profiles:

Syntax du fichier profiles.yml
------------------------------

Ce fichier est un tableau d'item dont les clé est ``PROFILES``.

Détail d'un item de configuration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

================ ================================= ===================================
Clé              Description                       Requis
================ ================================= ===================================
nom              Nom de la ressource               Obligatoire
prenom           Prénom de la ressource            Obligatoire
uid              Id Linux utiliser par le system   Obligatoire
                 (Dois être unique)                Obligatoire
shell            Chemin vers le shell à utiliser   Obligatoire
is_admin         Est un sudoers ?                  facultatif (par défault ``false``
login            Pour définir un login linux       facultatif (par défault c'est la
                 différent de celui calculé        première lettre du prénom concaténé
                                                   au nom
================ ================================= ===================================

Exemple de fichier

.. code-block:: yaml

    PROFILES :
      - { nom: BOUCHE,               prenom: Benjamin,  uid: 1101, shell: /bin/zsh }
      - { nom: HERMIN,               prenom: Xavier,    uid: 1109, shell: /bin/zsh, is_admin: true }
      - { nom: EL ALAOUI EL BEGHITI, prenom: Zakariae,  uid: 1111, shell: /bin/zsh, login: elalaoui }

Fonctionnalités du rôle
-----------------------

* Calcule la chaine de login si elle n'est pas précisé dans la configuration
* Calcule un mot de passe par défaut égale à ``[première lettre du prenom][nom]!123``
* Ajoute au group ``ADMINISTRATOR`` si la configuration *is_admin* est à *TRUE*
* Configure le SSH du compte utilisateur pour accéder à *gitlab-sicav*, *srvdev* et *sicav-preprod1*
* Créer la configuration git avec la configuration transmise par mettre __GIMPS__ GIT
* Créer le répertoire project pour l'utilisateur ``/data/sources/<initiales>`` et créer le lien symbolique dans son *home*
* Configure le shell
* Défini le umask à ``0002``
* Créer l'utilisateur samba avec le même mot de passe que linux


