============
Rôle Common
============


Ajout de dépots RPM complémentaire
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* rpmForge
* rhel-7-server-optional-rpms
* rhel-7-server-extras-rpms
* Mise à jour du system

Installation des packages
^^^^^^^^^^^^^^^^^^^^^^^^^^

* Outils de provisionning

  * parted
  * curl
  * wget

* Outils commun

  * zsh
  * moreutils
  * byobu
  * multitail
  * bind-utils
  * telnet
  * vim
  * tmux
  * tree

* Outil de compression

  * zip
  * unzip
  * bzip2
  * p7zip

* Installation des FileSystem

  * e2fsprogs
  * xfsprogs
  * btrfs-progs

* Autre

  * incron
  * python-pip

* Mise à jour de Rsync en >= 3.1
* Outils de developpement

  * jq
  * sloccount
  * tig

* Outils de status

  * htop
  * atop
  * iftop
  * mytop
  * iotop

* Outils system pour développeurs

  * fuse
  * fuse-sshfs
  * xorg-x11-server-Xvfb

* Outils de compilation

  * "@Development tools"
  * strace
  * gcc-c++
  * make

* Installation de langage complémentaire

  * ruby
  * ruby-devel

* Outil de conversion

  * poppler-utils
  * GraphicsMagick
  * ImageMagick
  * wkhtmltopdf

* Outil autres pour développeur

  * tnef
  * dos2unix
  * octave
  * lynx
  * links
  * mutt
  * p7zip
  * colordiff
  * graphviz

* Intallation de package ruby

  * compass
  * bundler

* Outil serveur

  * haveged

Configuration system
^^^^^^^^^^^^^^^^^^^^
* Configuration sysctl