.. _ref-role-ssh:

=============================
Rôle ssh
=============================

* Créer les groupes :
 * ADMINISTRATOR (groupe pour sudoers)
 * sshusers (groupe autorisé à faire du ssh à distance sur la machine)
* Ajoute l'utilisateur ``adminsol`` au groupe *sshusers*
* Ajoute l'utilisateur ``root`` au groupe *sshusers* (**deprecated**)
* Configure le ssh (sshd_config)
* Configure le sudoers
* S'assure que l'utilisateur de déploiement ``adminsol`` soir bien sudoers
* Ajoute les clé ssh générique à l'utilisateur ``adminsol``

.. important:: C'est ce rôle qui contient les clés public et privée de déploiement pour se connecter à l'utilisateur adminsol

.. tip:: les clés **id_rsa_deploy** peuvent être installer sur les différents postes
    utilisateurs amenés à ce connecter aux machines afin d'avoir une connection implicite


