=============
Projet deploy
=============

| Bienvenue sur la documentation Deploy pour Sicavonline.
|
| Ici vous trouverez des informations sur les scripts et procédure de configuration / Déploiement des serveur.
|


.. toctree::
   :caption: Documentation Deploy
   :maxdepth: 2

   content/Jenkins/index
   content/Ansible/index
   content/Sicavonline/index
   content/Process/index
