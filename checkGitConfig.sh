#!/bin/bash
users=(
	"dr"
	"hk"
	"b.bouche"
	"fj"
	"gm"
	"xh"
	"sb"
	"yg"
	"m.velandia"
	"v.rocard"
	"a.elkasmi"
	"zea"
	"j.legay"
	"dd"
	)
checks=(
	# "status"
	"name"
	"email"
	"autocrlf"
	"vimdiff"
	"HEAD@{u}..."
	# verifPresence 'default = simple' false
	# verifPresence 'ui = auto' false
	)

path="/home/httpd/"
debug=false

# ------------------------------------------------
# Fonction
# ------------------------------------------------
function checkGitConfig() {
	for user in ${users[@]}; do
		echo ""
		echo "***"
		echo "$user :"
		erreur=false;
		fichierGit="/home/$user/.gitconfig"

		sudo ls -l $fichierGit

		for check in ${checks[@]}; do
			verifPresence $check
		done
	done	
}

#
# $1 ce que l'on souhaite verifier
# $2 si l'on veut une erreur false si l'on ne veux que verifier la presence sans afficher le contenu
#
function verifPresence () {
	if [ $debug == true ]; then
		echo "verifPresence de $1"
	fi
	if [ "$2" != false ]; then
		sudo grep "$1" $fichierGit
	fi
	result=$(sudo grep "$1" $fichierGit | wc -l)
	if [ $result == 0 ]; then
		echo "La présence de '$1' n'a pas été trouvé"
	fi
}

# ------------------------------------------------
# Main
# ------------------------------------------------
if [ $(sudo whoami) != "root" ]; then
  echo "Votre utilisateur '$(whoami)' n'a pas les droits sudo."
  exit 1
fi

checkGitConfig