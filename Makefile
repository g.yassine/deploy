ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent
.PHONY: help list doc-html doc-dirhtml docker-build-deploy-test docker-deploy-test oracle-export-struct oracle-export-table-param check-gnome

PAPER                  =
HELP_SPACE             = 40
.DEFAULT_GOAL          := help   # Show help without arguments
BUILDDIR               = _build
PAPEROPT_a4            = -D latex_paper_size=a4
PAPEROPT_letter        = -D latex_paper_size=letter
ALLSPHINXOPTS          = -d $(BUILDDIR)/doctrees $(PAPEROPT_$(PAPER)) $(SPHINXOPTS) .
DOCKER_ORACLE          = "oracle_db"
ORACLE_INSTANCE        = "ORADEV"
EXTRACT_FILENAME_META  = "parfile_struct_METADATA.par"
EXTRACT_FILENAME_PARAM = "parfile_table_parametrage.par"

help: ## Aide explication de l'utilisation de ce Makefile
	@grep -E '^[a-zA-Z_/-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}'

list:
	sh -c "echo; $(MAKE) -p no_targets__ | awk -F':' '/^[a-zA-Z0-9][^\$$#\\t=]*:([^=]|$$)/ {split(\$$1,A,/ /);for(i in A)print A[i]}' | grep -v '__\$$' | grep -v 'Makefile'| sort"

documentation:
	docker run -t -i --rm -p 8080:8000 -v "$$(pwd)/documentation/docs/:/opt/docs" webdevops/sphinx sphinx-autobuild --poll -H 0.0.0.0 /opt/docs html

doc-html:
	docker run -t -i --rm -v "$$(pwd)/documentation/docs/:/opt/docs" -w /opt/docs webdevops/sphinx sphinx-build -b html $(ALLSPHINXOPTS) _build/html
	@echo "Build finished. The HTML pages are in _build/html."

doc-dirhtml:
	docker run -t -i --rm -v "$$(pwd)/documentation/docs/:/opt/docs" -w /opt/docs webdevops/sphinx sphinx-build -b dirhtml $(ALLSPHINXOPTS) _build/dirhtml
	@echo "Build finished. The HTML pages are in _build/dirhtml."

docker-build-deploy-test:
	docker build . -t registry.intranet.ageas.fr:5000/deploy-test:deploy-test-sicav

docker-deploy-test:
	docker run --rm registry.intranet.ageas.fr:5000/deploy-test:deploy-test-sicav goss validate

oracle-export-struct:
	@rm -f /data/oracle/instance/dpdump/${EXTRACT_FILENAME_META}
	@rm -f /data/oracle/instance/dpdump/metadata.dmp
	cp ${EXTRACT_FILENAME_META} /data/oracle/instance/dpdump;docker exec -ti ${DOCKER_ORACLE} expdp dp/zzb22x@${ORACLE_INSTANCE} parfile=/data/oracle/dpdump/${EXTRACT_FILENAME_META}
	@sudo chmod 0755 /data/oracle/instance/dpdump/metadata.dmp

oracle-export-table-param:
	@rm -f /data/oracle/instance/dpdump/${EXTRACT_FILENAME_PARAM}
	@rm -f /data/oracle/instance/dpdump/parametrage.dmp
	cp ${EXTRACT_FILENAME_PARAM} /data/oracle/instance/dpdump;docker exec -ti ${DOCKER_ORACLE} expdp dp/zzb22x@${ORACLE_INSTANCE} parfile=/data/oracle/dpdump/${EXTRACT_FILENAME_PARAM}
	@sudo chmod 0755 /data/oracle/instance/dpdump/parametrage.dmp

check-gnome: ## Check avec ansible si gnome est en cours d'execution sur les serveurs
	cd playbook ; ansible -i hosts new \
		-m shell \
		-a "sudo ps a |grep -v 'gnom'"

check-prerequis: ## Check si le nouveau serveur est operationnel
	cd playbook ; PASSWD=$(cat passwd) ; ansible_sudo_pass=${PASSWD} ; export ansible_sudo_pass ;\
	ansible-playbook -i hosts \
		--limit sicav-preprod3 \
		--extra-vars "ansible_sudo_pass=${PASSWD}" \
		prerequisiteServers.yml

install-logwatch: ## install logwatch sur les serveurs dockerises
	cd playbook ; PASSWD=$(cat passwd) ; ansible_sudo_pass=${PASSWD} ; export ansible_sudo_pass ;\
	ansible-playbook -i hosts \
		--extra-vars "ansible_sudo_pass=${PASSWD}" \
		installLogwatch.yml

install-docker: ## install docker sur les serveurs dockerises
	# LIMIT_SERVERS=sicav-preprod3 ;\
	# echo "install docker sur les serveurs : ${LIMIT_SERVERS}"; \

	cd playbook ; PASSWD=$(cat passwd) ; ansible_sudo_pass=${PASSWD} ; export ansible_sudo_pass ; \
	ansible-playbook -i hosts \
		--limit sicav-preprodb \
		--extra-vars "ansible_sudo_pass=${PASSWD}" \
		installDocker.yml

install-server: ## installation d'un serveur dockerise pour y faire tourner differents services
	cd playbook ; \
	ansible-playbook -i hosts \
	webservers.yml

restart-install-server: ## installation d'un serveur dockerise pour y faire tourner differents services
	cd playbook ; \
	ansible-playbook -i hosts \
	--start-at-task="Fix the certificat error x509: certificate signed by unknown authority" \
	webservers.yml
