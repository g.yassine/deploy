FROM registry.intranet.ageas.fr:5000/php-apache-dev:latest

LABEL maintainer "Stéphane SALLES <s.salles@sicavonline.fr>"

ENV HOME_DIR=/home/git \
    HTTP_DIR=/home/httpd \
    BIN_DIR=/home/app/bin \
    PHP_DIR=/usr/local/php-5.6.10/bin \
    COMPOSER_DIR=/home/app/bin \
    DEPLOY_DIR=/home/app/deploy

RUN /usr/local/bin/yum-install \
  sudo \
  mailx \
  && mkdir -p $HTTP_DIR $PHP_DIR $COMPOSER_DIR $DEPLOY_DIR \
  && curl -fsSL https://goss.rocks/install | sh \
  && groupadd git \
  && groupadd info \
  && useradd git -g git -g wheel -g info \
  && echo "Cmnd_Alias DEPLOY = /bin/chown, /bin/rm, /bin/cp, /bin/whoami, /bin/mail, /bin/chmod" >> /etc/sudoers \
  && echo "git     ALL=(ALL)       NOPASSWD: DEPLOY" >> /etc/sudoers \
  && chown -R git:info $BIN_DIR \
  && chown -R git:info $HTTP_DIR \
  && chown -R git:info $COMPOSER_DIR \
  && chown -R git:git $DEPLOY_DIR \
  && chmod -R 0777 $DEPLOY_DIR \
  && wget http://www.phing.info/get/phing-latest.phar -O $BIN_DIR/phing.phar \
  && wget https://getcomposer.org/download/1.5.1/composer.phar -O $COMPOSER_DIR/composer.phar \
  && mkdir $HOME_DIR/.ssh \
  && ln -s $(which php) $PHP_DIR/php

COPY ./deployDev.sh $HOME_DIR/deployDev.sh
COPY ./tests/assets/.ssh/id_rsa $HOME_DIR/.ssh/id_rsa
COPY ./tests/assets/.ssh/config $HOME_DIR/.ssh/config
COPY ./tests/assets/.ssh/known_hosts $HOME_DIR/.ssh/known_hosts

RUN chown -R git:git $HOME_DIR \
  && chmod 0600 $HOME_DIR/.ssh/id_rsa

USER git

# Création de jeux de test, de vieux déploiements
RUN $HOME_DIR/deployDev.sh deploy develop \
  && $HOME_DIR/deployDev.sh \
  && $HOME_DIR/deployDev.sh backoffice master \
  && cp $HTTP_DIR/backoffice/config/parameters.dist.cfm $HTTP_DIR/backoffice/config/parameters.cfm \
  && cp $HTTP_DIR/backoffice/config/parameters.dist.php $HTTP_DIR/backoffice/config/parameters.php \
  && $HOME_DIR/deployDev.sh deploy develop \
  && $HOME_DIR/deployDev.sh ariane develop \
  && cp $HTTP_DIR/ariane/app/config/parameters.yml.dist $HTTP_DIR/ariane/app/config/parameters.yml \
  && $HOME_DIR/deployDev.sh backoffice master \
  && $HOME_DIR/deployDev.sh deploy develop \
  && $HOME_DIR/deployDev.sh ariane develop \
  && $HOME_DIR/deployDev.sh

WORKDIR $HOME_DIR

VOLUME ["$HOME_DIR"]

COPY ./tests/assets/goss.yaml $HOME_DIR/goss.yaml

ENV USER git
USER git

CMD [ "goss" "validate" ]

