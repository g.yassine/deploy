#!/bin/bash

projects=(
#  "backoffice"
  "php_commun"
)
projectsItSol=(
  "trash"
  "documentation"
  "Dockerfile"
  "example"
  "tpSymfony"
  "tools"
  "droits"
  "vagrant"
  "deploy"
  "Font-Awesome"
)
projectsSicav=(
  "studio_cfm"
  "nc2"
  "avenir"
  "scpi"
  "php_commun"
  "adyal"
  "tmptrash"
  "studio"
  "portail"
  "part"
  "exemple"
  "bo"
  "backoffice"
  "1688"
  "ariane"
  "core"
  "CoreBundle"
  "test"
  "afim"
  "afin"
  "studio_non_use"
  "trashSymfony"
)
dirSources="/home/git/"
dirDestination="./transfert/"

gitlabConf="git@gitlab-sicav:"

SRC_REMOTE=origin
DST_REMOTE=gitlab

colorNormal="\033[0m"
colorGreen="\033[32m"
colorRed="\033[31m"
colorYellow="\033[33m"
colorBlue="\033[34m"
colorCyan="\033[36m"
# ------------------------------------------------
# Functions
# ------------------------------------------------
parseNomProjet () {
  echo $(cat $1 |grep gitProject=\" | awk '{print substr($0, 12)}' | sed -e "s/\"//g")
}

displayMsg () {
  # determine le préfixe selon le 2em paramètre
  case $2 in
      true ) prefixe=$colorGreen ;;
      false ) prefixe=$colorRed ;;
      "alert" ) prefixe="$colorYellow/!\ " ;;
      "green" ) prefixe=$colorGreen ;;
      "info" ) prefixe="$colorYellow " ;;
      "blue" ) prefixe="$colorBlue " ;;
      "cyan" ) prefixe="$colorCyan " ;;
      *) prefixe="" ;;
  esac

  lineLog="$prefixe$1"

  case $2 in
      true|false|"alert"|"info"|"green"|"blue"|"cyan" ) lineLog="$prefixe$1$colorNormal" ;;
      *) lineLog="$prefixe$1" ;;
  esac

  if [ "$2" == true ]; then
   echo "---" 
  fi
  echo -e "$lineLog"
}

# ------------------------------------------------
# Main
# ------------------------------------------------
# si le parametre 1 est --help alors affiche l'aide

if [ "$1" = "--help" ] || [ "$1" = "-h" ] || [ $# -ne 1 ]; then
    if [ $# -ne 1 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
    transfertGit.sh - (2017-02-10)
    ==============================

    Transfert le repository actuel sur le serveur gitlab (ne désactive pas le repo d'origine)

    Attention l'utilisateur doit avoir les droits sudo

    Parametres :
    nomDuProjetGit : projet à transferer

    Exemple d'utilisation :

    $ ./transfertGit.sh [nomDuProjetGit]
EOF
    exit 0
fi

if [ $(sudo whoami) != "root" ]; then
  echo "Votre utilisateur '$(whoami)' n'a pas les droits sudo."
  exit 1
fi

# Affichage des repos existant (non-archivés)
ls -d $dirSources*.git|grep -v '_old'
echo "Création du dossier : $dirDestination"
mkdir -p $dirDestination

for project in ${projects[@]}; do

  repo="${dirSources}${project}.git"
  repOld="${dirSources}${project}_old.git"
  dirDestProjet="${dirDestination}${project}"
  
  # si le repertoire n'existe pas 
  if [ -e "$repo" ]; then
    displayMsg "Traitement du $repo :" false
    # clone le depo
    if [ ! -e $dirDestProjet ]; then
      git clone $repo $dirDestProjet
    fi

    # entre dans le nouveau depo
    cd $dirDestProjet

    # clone mirror le depo
    case $project in
      ${projectsItSol} ) prefixProject="it-sol/" ;;
      ${projectsSicav} ) prefixProject="sicav/" ;;
      *) prefixProject="" ;;
    esac
    gitDest="${gitlabConf}${prefixProject}${project}.git"
    displayMsg "git dest : $gitDest" true
    git remote add $DST_REMOTE $gitDest

    # apres avoir ajouté le repo gitlab lui transfert tout
    displayMsg ">>> Branches : "
    for a in $(git branch --list --remote "$SRC_REMOTE/*" | grep -v --regexp='->')
      do 
      displayMsg "-> $a ($DST_REMOTE $a:refs/heads/${a//$SRC_REMOTE\/})" "cyan" 
      git push "$DST_REMOTE" "$a:refs/heads/${a//$SRC_REMOTE\/}"
    done

    # recupere tous les tags pour les transferer à gitlab
    displayMsg ">>> Tags : $(git tag --list|wc -l) tags"
    git fetch --tags
    git push --tags $DST_REMOTE


    # on annule le traitement fait
    cd -
    rm -rf $dirDestProjet

    if [ "$2" != "maj" ]; then
      # on renome le repo en old
      displayMsg ">>> Desactivation du repo : $repOld"
      cmd="sudo mv $repo $repOld"
      displayMsg $cmd
      $($cmd)
    fi
  else
    displayMsg "Le repo '$repo' n'existe pas !" alert
  fi

done

exit 0
