#!/bin/bash

projects=(
  "/home/git/1688.git"
  "/home/git/adyal.git"
  "/home/git/afim.git"
  "/home/git/afin.git"
  "/home/git/avenir.git"
  "/home/git/backoffice.git"
  "/home/git/bo.git"
  "/home/git/docker.git"
  "/home/git/exemple.git"
  "/home/git/nc2.git"
  "/home/git/part.git"
  "/home/git/php_commun.git"
  "/home/git/portail.git"
  "/home/git/scpi.git"
  "/home/git/studio_cfm.git"
  "/home/git/studio.git"
  "/home/git/thes.git"
  "/home/git/tools.git"
  "/home/git/tpSymfony.git"
  "/home/git/trash.git"
  "/home/git/trashSymfony.git"
  "/home/git/vagrant.git"
)
fichierSource="./hooks/post-receive"

parseNomProjet () {
  echo $(cat $1 |grep gitProject=\" | awk '{print substr($0, 12)}' | sed -e "s/\"//g")
}
nomSource=$(parseNomProjet $fichierSource)

# ------------------------------------------------
# Main
# ------------------------------------------------
if [ $(whoami) != "git" ]; then
  echo "Vous devez utiliser ce programme en tant que l'utilisateur git"
  exit 1
fi

if [ $(sudo whoami) != "root" ]; then
  echo "Votre utilisateur '$(whoami)' n'a pas les droits sudo."
  exit 1
fi

for project in ${projects[@]}; do
  hook="$project/hooks/post-receive"
  if [ -f $hook ]; then
    sudo chown git: $hook
    nomProjet=$(parseNomProjet $hook)
    echo -e "$nomProjet\t\t: $hook"
    cp $fichierSource $hook
    
    sed -i "s/$nomSource/$nomProjet/g" $hook
  fi
done

exit 0
