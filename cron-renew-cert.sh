#!/usr/bin/env bash
# Bash3 Boilerplate. Copyright (c) 2014, kvz.io

set -o errexit
set -o pipefail
set -o nounset
# set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename ${__file} .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)" # <-- change this as it depends on your app

arg1="${1:-}"
PATH_PROJECT="${arg1:-/root/.acme.sh}"
MAIL_DEST=s.salles@sicavonline.fr,devops@sicavonline.fr,technical@sicavonline.fr,reseaux.telecom@ageas.fr
MAIL_FROM="cron-renew-cert <root@$(uname -n)>"
PATH_TAR=/tmp/archive-cert.tar
PATH_TMP_CORE_MAIL=/tmp/coreMail.txt
PASSWORD=$(cat ${__dir}/cron-renew-cert-passwd)
DNSs=(
    'backoffice5.sicavonline.fr'
    'backoffice4.sicavonline.fr'
    'souscription.sicavonline.fr'
)

if [ "$arg1" = "--help" ] || [ "$arg1" = "-h" ] || [ $# -gt 1 ]; then
    if [ $# -gt 1 ]; then
        echo "Le nombre d'arguments est invalide"
        echo ""
    fi
    cat << EOF
    cron-renew-cert.sh - (2018 May 18)
    ==================================
    Génére les certificats pfx pour être exploités pour inspection SSL

    Exemple d'utilisation :
    $ cron-renew-cert.sh
EOF
    if [ $# -gt 1 ]; then
        exit 1
    else
        exit 0
    fi
fi

if [ ! -d ${PATH_PROJECT} ]; then
    echo "Le dossier spécifié pour le PATH_PROJECT n'existe pas : ${PATH_PROJECT}"
    exit 1
fi

rm ${PATH_TAR} || true

for DNS in "${DNSs[@]}"; do
    if [ -d ${PATH_PROJECT}/${DNS} ]; then
        cd ${PATH_PROJECT}/${DNS}

        echo "---"
        echo "Génération du certificat pour ${DNS} :"
        if [ -e ${DNS}.cer ] && [ -e ca.cer ] && [ -e ${DNS}.key ]; then
            echo "  openssl pkcs12 -in ${DNS}.cer -certfile ca.cer -inkey ${DNS}.key -password pass:******* -export -out ${DNS}.pfx"
            openssl pkcs12 -in ${DNS}.cer -certfile ca.cer -inkey ${DNS}.key -password pass:${PASSWORD} -export -out ${DNS}.pfx
            elif [ -e privkey.pem ] && [ -e cert.pem ]; then
            echo "  openssl pkcs12 -inkey privkey.pem -in cert.pem -password pass:******* -export -out ${DNS}.pfx"
            openssl pkcs12 -inkey privkey.pem -in cert.pem -password pass:${PASSWORD} -export -out ${DNS}.pfx
        fi

        if [ -e ${DNS}.pfx ]; then
            echo ""
            echo "Résultats :"
            ls -lh ${DNS}.pfx

            tar -rvf ${PATH_TAR} ${DNS}.pfx
        fi
    fi
done

echo ""
echo "Liste des certificats archivés dans ${PATH_TAR} :"
ls -lh ${PATH_TAR}
tar -tvf ${PATH_TAR}

echo ""
echo "Envoi du mail à ${MAIL_DEST-s.salles@sicavonline.fr}"
echo "Vous trouverez ci joint les cerficats générés ce jour." > ${PATH_TMP_CORE_MAIL}
echo "" >> ${PATH_TMP_CORE_MAIL}
if [ -e /root/.acme.sh/acme.sh ]; then
    /root/.acme.sh/acme.sh --list >> ${PATH_TMP_CORE_MAIL}
fi
cat ${PATH_TMP_CORE_MAIL} | EMAIL=${MAIL_FROM} mutt -a "${PATH_TAR}" -s "Certificats du $(uname -n) le $(date)" -- ${MAIL_DEST-s.salles@sicavonline.fr}
rm ${PATH_TMP_CORE_MAIL}

exit 0
